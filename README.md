# PV168 Project: Cash Flow Evidence

The desktop application will allow the customer to track his personal cash flow. He wants to know how much money he receives and spends, when and for what.

## Team Information

| Seminar Group | Team |
|-------------- | ---- |
| PV168/02      | 4   |

### Members

| Role           | Person               |
|----------------|----------------------|
|Team Lead       | [Jakub Judiny](https://is.muni.cz/auth/osoba/525230) |
|Member          | [Martin Suchánek](https://is.muni.cz/auth/osoba/514540) |
|Member          | [Sophia Zápotočná](https://is.muni.cz/auth/osoba/514318) |
|Member          | [Adam Krídl](https://is.muni.cz/auth/osoba/492892) |

### Evaluators

| Role           | Person               |
|----------------|----------------------|
|Customer        | [Tomáš Ondruško](https://is.muni.cz/auth/osoba/485418) |
|Technical Coach | [Imrich Nagy](https://is.muni.cz/auth/osoba/456271) |


