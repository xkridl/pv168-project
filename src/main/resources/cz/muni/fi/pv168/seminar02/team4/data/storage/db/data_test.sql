--
-- Test data - used for testing
--

--
-- Categories
--
INSERT INTO Category(name, colorRed, colorGreen, colorBlue)
VALUES ('Healthcare', 255, 0, 0),
       ('Culture', 0, 255, 0),
       ('Sport', 128, 128, 128),
       ('Living', 255, 255, 0);

--
-- Currencies
--
INSERT INTO Currency(name, abbreviation, conversionRate)
VALUES ('Dollar', 'USD', 0.97);
