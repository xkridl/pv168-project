--
-- Category table definition
--
CREATE TABLE IF NOT EXISTS Category
(
    id         BIGINT GENERATED ALWAYS AS IDENTITY,
    name       VARCHAR(50),
    colorRed   SMALLINT,
    colorGreen SMALLINT,
    colorBlue  SMALLINT,
    createdAt  TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);

--
-- Currency table definition
--
CREATE TABLE IF NOT EXISTS Currency
(
    id             BIGINT GENERATED ALWAYS AS IDENTITY,
    name           VARCHAR(30),
    abbreviation   VARCHAR(3),
    conversionRate REAL
);

--
-- Transaction table definition
--
CREATE TABLE IF NOT EXISTS Transaction
(
    id              BIGINT GENERATED ALWAYS AS IDENTITY,
    categoryId      BIGINT, -- REFERENCES Category(id),
    currencyId      BIGINT, -- REFERENCES Currency(id),
    name            VARCHAR(50),
    description     VARCHAR(200),
    amount          REAL,
    date            DATE,
    transactionType ENUM('Incoming', 'Outgoing'),
    period          VARCHAR(20)
);

---------------------------------------

--
-- Category table (named) constraints
--
ALTER TABLE IF EXISTS PUBLIC.Category
    ADD CONSTRAINT IF NOT EXISTS CATEGORY_PK
        PRIMARY KEY(id);

ALTER TABLE IF EXISTS PUBLIC.Category
    ADD CONSTRAINT IF NOT EXISTS CATEGORY_NAME_NOT_NULL
        CHECK (Category.name IS NOT NULL);

ALTER TABLE IF EXISTS PUBLIC.Category
    ADD CONSTRAINT IF NOT EXISTS CATEGORY_NAME_UC
        UNIQUE(name);

ALTER TABLE IF EXISTS PUBLIC.Category
    ADD CONSTRAINT IF NOT EXISTS CATEGORY_RED_NOT_NULL
        CHECK (Category.colorRed IS NOT NULL);

ALTER TABLE IF EXISTS PUBLIC.Category
    ADD CONSTRAINT IF NOT EXISTS CATEGORY_GREEN_NOT_NULL
        CHECK (Category.colorGreen IS NOT NULL);

ALTER TABLE IF EXISTS PUBLIC.Category
    ADD CONSTRAINT IF NOT EXISTS CATEGORY_BLUE_NOT_NULL
        CHECK (Category.colorBlue IS NOT NULL);

ALTER TABLE IF EXISTS PUBLIC.Category
    ADD CONSTRAINT IF NOT EXISTS CATEGORY_CREATED_AT_NOT_NULL
        CHECK (Category.createdAt IS NOT NULL);

--
-- Currency table (named) constraints
--
ALTER TABLE IF EXISTS PUBLIC.Currency
    ADD CONSTRAINT IF NOT EXISTS CURRENCY_PK
        PRIMARY KEY(id);

ALTER TABLE IF EXISTS PUBLIC.Currency
    ADD CONSTRAINT IF NOT EXISTS CURRENCY_NAME_NOT_NULL
        CHECK (Currency.name IS NOT NULL);

ALTER TABLE IF EXISTS PUBLIC.Currency
    ADD CONSTRAINT IF NOT EXISTS CURRENCY_NAME_UC
        UNIQUE(name);

ALTER TABLE IF EXISTS PUBLIC.Currency
    ADD CONSTRAINT IF NOT EXISTS CURRENCY_ABBREVIATION_NOT_NULL
        CHECK (Currency.abbreviation IS NOT NULL);

ALTER TABLE IF EXISTS PUBLIC.Currency
    ADD CONSTRAINT IF NOT EXISTS CURRENCY_ABBREVIATION_UC
        UNIQUE(abbreviation);

ALTER TABLE IF EXISTS PUBLIC.Currency
    ADD CONSTRAINT IF NOT EXISTS CURRENCY_CONVERSION_RATE_NOT_NULL
        CHECK (Currency.conversionRate IS NOT NULL);

--
-- Transaction table (named constraints)
--
ALTER TABLE IF EXISTS PUBLIC.Transaction
    ADD CONSTRAINT IF NOT EXISTS TRANSACTION_PK
        PRIMARY KEY(id);

ALTER TABLE IF EXISTS PUBLIC.Transaction
    ADD CONSTRAINT IF NOT EXISTS TRANSACTION_CATEGORY_FK
        FOREIGN KEY(categoryId) REFERENCES Category(id);

ALTER TABLE IF EXISTS PUBLIC.Transaction
    ADD CONSTRAINT IF NOT EXISTS TRANSACTION_CURRENCY_FK
        FOREIGN KEY(currencyId) REFERENCES Currency(id);

ALTER TABLE IF EXISTS PUBLIC.Transaction
    ADD CONSTRAINT IF NOT EXISTS TRANSACTION_NAME_NOT_NULL
        CHECK(name IS NOT NULL);

ALTER TABLE IF EXISTS PUBLIC.Transaction
    ADD CONSTRAINT IF NOT EXISTS TRANSACTION_AMOUNT_NOT_NULL
        CHECK(amount IS NOT NULL);

ALTER TABLE IF EXISTS PUBLIC.Transaction
    ADD CONSTRAINT IF NOT EXISTS TRANSACTION_DATE_NOT_NULL
        CHECK("date" IS NOT NULL);

ALTER TABLE IF EXISTS PUBLIC.Transaction
    ADD CONSTRAINT IF NOT EXISTS TRANSACTION_TYPE_NOT_NULL
        CHECK(transactionType IS NOT NULL);

ALTER TABLE IF EXISTS PUBLIC.Transaction
    ADD CONSTRAINT IF NOT EXISTS TRANSACTION_PERIOD_NOT_NULL
        CHECK(period IS NOT NULL);

------------------------------------------------
--
-- Insert currencies Euro and CZK
--
INSERT INTO Currency(name, abbreviation, conversionRate)
SELECT 'Euro', 'EUR', 1.0
WHERE NOT EXISTS(SELECT * FROM Currency WHERE abbreviation='EUR');

INSERT INTO Currency(name, abbreviation, conversionRate)
SELECT 'Czech crown', 'CZK', 0.04
WHERE NOT EXISTS(SELECT * FROM Currency WHERE abbreviation='CZK');

--
-- Insert category "No category"
--
INSERT INTO Category(name, colorRed, colorGreen, colorBlue)
SELECT 'No category', 255, 255, 255
WHERE NOT EXISTS(SELECT * FROM Category WHERE name='No category');
