--
-- Development data - used for development
--

--
-- Categories
--
INSERT INTO Category(id, name, colorRed, colorGreen, colorBlue)
VALUES ('Food', 0, 0, 255),
       ('Healthcare', 255, 0, 0),
       ('Culture', 0, 255, 0),
       ('Sport', 128, 128, 128),
       ('Living', 255, 255, 0),
       ('No category', 255, 255, 255)
;

--
-- Currencies
--
INSERT INTO Currency(name, abbreviation, conversionRate)
VALUES ('Euro', 'EUR', 1.0),
       ('Dollar', 'USD', 0.97),
       ('Czech crown', 'CZK', 0.04)
;

--
-- Transactions
--
-- INSERT INTO Transaction(categoryId, currencyId, name, description, amount, date, transactionType, period)
-- VALUES
-- ;
