package cz.muni.fi.pv168.seminar02.team4.data.manipulation;

import cz.muni.fi.pv168.seminar02.team4.model.Transaction;
import org.json.simple.JSONObject;

import javax.swing.*;
import java.awt.*;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

class JsonTransactionExporter implements TransactionExporter {
    private Component parentComponent;
    public JsonTransactionExporter(Component parentComponent) {
        this.parentComponent = parentComponent;
    }
    @Override
    public void exportTransactions(Collection<Transaction> transactions, String filePath) {
        var transactionsAsJsonArray = new ArrayList<>();
        for (var transaction : transactions) {
            transactionsAsJsonArray.add(transaction.toJSON());
        }
        var object = new HashMap<>();
        object.put("transactions", transactionsAsJsonArray);
        var jsonObject = new JSONObject(object);

        try (var writer = new BufferedWriter(new FileWriter(filePath, StandardCharsets.UTF_8))) {
            writer.write(jsonObject.toJSONString());
            writer.newLine();
        } catch (IOException exception) {
            JOptionPane.showMessageDialog(parentComponent,
                    "The application was unable to write to that file.",
                    "Export", JOptionPane.ERROR_MESSAGE);
        }
    }
}
