package cz.muni.fi.pv168.seminar02.team4.ui.action;

import cz.muni.fi.pv168.seminar02.team4.data.storage.DataStorageException;
import cz.muni.fi.pv168.seminar02.team4.logic.BusinessLogicImpl;
import cz.muni.fi.pv168.seminar02.team4.model.Category;
import cz.muni.fi.pv168.seminar02.team4.model.ValidationInfo;
import cz.muni.fi.pv168.seminar02.team4.ui.dialog.CategoryDialog;
import cz.muni.fi.pv168.seminar02.team4.ui.resources.Icons;
import org.tinylog.Logger;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JOptionPane;
import javax.swing.KeyStroke;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.util.Optional;

public class AddCategoryAction extends AbstractAction implements Action {
    private final Component parentComponent;

    public AddCategoryAction(Component parentComponent) {
        super("Add", Icons.ADD_CATEGORY_ICON);
        this.parentComponent = parentComponent;

        putValue(SHORT_DESCRIPTION, "Adds new category");
        putValue(MNEMONIC_KEY, KeyEvent.VK_A);
        putValue(ACCELERATOR_KEY, KeyStroke.getKeyStroke("ctrl N"));
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        var categoryDialog = new CategoryDialog("New category");
        Optional<Category> newCategory;

        while (true) {
            newCategory = categoryDialog.show(parentComponent);
            if (newCategory.isEmpty()) {
                return;
            }

            var category = newCategory.get();
            ValidationInfo validationInfo = category.validate();
            if (validationInfo.isInvalid()) {
                JOptionPane.showMessageDialog(parentComponent,
                        validationInfo.getMessage(),
                        "Category", JOptionPane.ERROR_MESSAGE);
            } else {
                try {
                    Logger.debug("Adding category: {}", category.detailedPrint());
                    BusinessLogicImpl.getInstance().addCategory(category);
                    return;
                } catch (DataStorageException ex) {
                    Logger.error(ex);
                    JOptionPane.showMessageDialog(parentComponent,
                            "Unable to add such a category. Check if the name is valid and not used.",
                            "Category", JOptionPane.ERROR_MESSAGE);
                }
            }
        }
    }
}
