package cz.muni.fi.pv168.seminar02.team4.ui.model;

import cz.muni.fi.pv168.seminar02.team4.model.Category;

import java.awt.Color;

/**
 * Concrete subclass of the {@link ListeningListModelWithAnyDecorator} which works
 * with {@link Category} type inside.
 *
 * @author Adam Kridl
 */
public class CategoryListeningListModelDecorator extends ListeningListModelWithAnyDecorator<Category> {
    public CategoryListeningListModelDecorator(ListeningListModel<Category> categories) {
        super(categories);
    }

    @Override
    public Category getElementWithLabelAny() {
        return new Category("Any", Color.BLACK);
    }
}
