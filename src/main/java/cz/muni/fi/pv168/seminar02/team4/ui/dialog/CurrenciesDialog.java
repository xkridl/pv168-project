package cz.muni.fi.pv168.seminar02.team4.ui.dialog;

import cz.muni.fi.pv168.seminar02.team4.data.storage.DataStorageException;
import cz.muni.fi.pv168.seminar02.team4.data.storage.repository.CurrencyRepository;
import cz.muni.fi.pv168.seminar02.team4.logic.BusinessLogicImpl;
import cz.muni.fi.pv168.seminar02.team4.model.Amount;
import cz.muni.fi.pv168.seminar02.team4.model.Currency;
import cz.muni.fi.pv168.seminar02.team4.model.Transaction;
import cz.muni.fi.pv168.seminar02.team4.model.ValidationInfo;
import cz.muni.fi.pv168.seminar02.team4.ui.model.ComboBoxModelAdapter;
import cz.muni.fi.pv168.seminar02.team4.ui.model.ListeningListModel;
import cz.muni.fi.pv168.seminar02.team4.ui.model.ListeningListModelImpl;
import cz.muni.fi.pv168.seminar02.team4.ui.resources.Icons;
import org.tinylog.Logger;

import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import java.awt.*;
import java.awt.event.*;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

import static javax.swing.JOptionPane.CANCEL_OPTION;
import static javax.swing.JOptionPane.PLAIN_MESSAGE;

public class CurrenciesDialog extends EntityDialog<Currency> {
    private JLabel changesTooltip;
    private final JComponent parentComponent;
    private final CurrencyRepository currencyRepository;
    private final ListeningListModel<Currency> currencyModel;
    private final JComboBox<Currency> currencyComboBox;
    private JTextField nameField;
    private JTextField abbreviationField;
    private JTextField conversionRateField;

    public CurrenciesDialog(JComponent parentComponent) {
        super("gap 30px 10px", "Currencies");
        this.parentComponent = parentComponent;
        this.currencyRepository = BusinessLogicImpl.getInstance().getCurrencyRepository();
        currencyModel = new ListeningListModelImpl<>();
        BusinessLogicImpl.getInstance().addCurrencyListener(currencyModel);
        currencyComboBox = new JComboBox<>(new ComboBoxModelAdapter<>(currencyModel));
        panel.add(currencyComboBox, "w 50%");

        createButtonPanel();
        setDialogItems();
        addDialogItems();
        addActionListener();

        panel.setPreferredSize(new Dimension(350, panel.getPreferredSize().height));
    }

    public Optional<Currency> show(Component parent) {
        String[] options = {"Cancel"};
        JOptionPane.showOptionDialog(parent, panel, "Currencies",
                CANCEL_OPTION, PLAIN_MESSAGE, null, options, options[0]);
        return Optional.empty();
    }

    private void setDialogItems() {
        if (currencyComboBox.getItemCount() > 0) {
            currencyComboBox.setSelectedItem(currencyModel.getElementAt(0));
        }

        var currency = (Currency) currencyComboBox.getSelectedItem();
        if (currency == null) {
            nameField = new JTextField("");
            abbreviationField = new JTextField("");
            conversionRateField = new JTextField("");
        } else {
            nameField = new JTextField(currency.getName());
            abbreviationField = new JTextField(currency.getAbbreviation());
            conversionRateField = new JTextField(String.format("%.2f", currency.getConversionRate()));

            if (abbreviationField.getText().equals("EUR")) {
                abbreviationField.setEnabled(false);
                conversionRateField.setEnabled(false);
                conversionRateField.setBackground(abbreviationField.getBackground());
            }
        }
    }

    public JPanel createButtonPanel() {
        var buttonPanel = new JPanel();

        changesTooltip = new JLabel("<HTML>Unsaved</HTML>");
        changesTooltip.setForeground(Color.red);
        changesTooltip.setVisible(false);
        buttonPanel.add(changesTooltip, "wrap");

        var saveButton = new JButton(Icons.SAVE_ICON);
        saveButton.addActionListener(this::save);
        saveButton.setToolTipText("Save");
        buttonPanel.add(saveButton, "wrap, align right");

        var deleteButton = new JButton(Icons.DELETE_ICON);
        deleteButton.addActionListener(this::deleteCurrency);
        deleteButton.setToolTipText("Delete");
        buttonPanel.add(deleteButton, "wrap, align right");

        var addButton = new JButton(Icons.ADD_ICON);
        addButton.addActionListener(this::openAddCurrencyDialog);
        addButton.setToolTipText("Add new");
        buttonPanel.add(addButton, "wrap, align right");
        panel.add(buttonPanel, "wrap, align right");

        return buttonPanel;
    }

    private void save(ActionEvent actionEvent) {
        Currency chosenEntity = (Currency) currencyComboBox.getSelectedItem();
        if (chosenEntity == null)
        {
            return;
        }

        double conversionRate;
        try {
            conversionRate = Double.parseDouble(conversionRateField.getText().replace(",", "."));
        } catch (NumberFormatException ex) {
            JOptionPane.showMessageDialog(parentComponent,
                    "Conversion rate has invalid format. Try again.",
                    "Currency", JOptionPane.ERROR_MESSAGE);
            return;
        }


        Currency newEntity = new Currency(nameField.getText(), abbreviationField.getText(), conversionRate);
        ValidationInfo validationInfo = newEntity.validate();
        if (validationInfo.isInvalid())
        {
            JOptionPane.showMessageDialog(parentComponent,
                    validationInfo.getMessage(),
                    "Currency", JOptionPane.ERROR_MESSAGE);
            return;
        }

        Currency oldCurrency = chosenEntity.createSnapshot();
        chosenEntity.setName(newEntity.getName());
        chosenEntity.setAbbreviation(newEntity.getAbbreviation());
        chosenEntity.setConversionRate(newEntity.getConversionRate());
        try {
            currencyRepository.update(chosenEntity);
            BusinessLogicImpl.getInstance().recomputeBalance();
        } catch (DataStorageException exception) {
            JOptionPane.showMessageDialog(parentComponent,
                    //exception.getCause().getMessage()
                    "Currency with abbreviation '" + chosenEntity.getAbbreviation() + "' already exists." ,
                    "Currency", JOptionPane.ERROR_MESSAGE);

            chosenEntity.setAttributes(oldCurrency);
        }

        changesTooltip.setVisible(false);
    }

    private void addDocumentListener(JTextField textField) {
        textField.getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent e) {
                update();
            }
            @Override
            public void removeUpdate(DocumentEvent e) {
                update();
            }
            @Override
            public void changedUpdate(DocumentEvent e) {
                update();
            }
            private void update() {
                changesTooltip.setVisible(true);
            }
        });
    }

    private void addActionListener() {
        ActionListener actionListener = actionEvent -> {
            if (currencyModel.getSize() == 0) {
                nameField.setText("");
                abbreviationField.setText("");
                conversionRateField.setText("");
            } else {
                Currency currency = (Currency) currencyComboBox.getSelectedItem();
                nameField.setText(Objects.requireNonNull(currency).getName());
                abbreviationField.setText(currency.getAbbreviation());
                conversionRateField.setText(String.format("%.2f", currency.getConversionRate()));
                conversionRateField.setBackground(BusinessLogicImpl.getInstance().getTextFieldMainColor());

                if (currency.getAbbreviation().equals("EUR")) {
                    abbreviationField.setEnabled(false);
                    conversionRateField.setEnabled(false);
                    conversionRateField.setBackground(abbreviationField.getBackground());
                } else {
                    abbreviationField.setEnabled(true);
                    conversionRateField.setEnabled(true);
                }
                changesTooltip.setVisible(false);

            }
        };
        currencyComboBox.addActionListener(actionListener);
    }

    @Override
    void addDialogItems() {
        addDocumentListener(nameField);
        addDocumentListener(abbreviationField);
        addDocumentListener(conversionRateField);

        add("Name", nameField, true, "w 40%");
        panel.add(nameField, "w 60%, wrap, align right");
        add("Abbreviation", abbreviationField, true, "w 40%");
        panel.add(abbreviationField, "w 60%, wrap, align right");
        add("Conversion Rate", conversionRateField, true, "w 40%");
        conversionRateField.addKeyListener(floatFieldListener(conversionRateField));
        panel.add(conversionRateField, "w 60%, wrap, align right");
    }

    @Override
    Currency getEntity() {
        return (Currency) currencyComboBox.getSelectedItem();
    }

    private void openAddCurrencyDialog(ActionEvent e) {
        var dialog = new AddCurrencyDialog("New Currency");
        Optional<Currency> newCurrency;

        outerloop:
        while (true) {
            try {
                newCurrency = dialog.show(parentComponent);
            } catch (NumberFormatException ex) {
                JOptionPane.showMessageDialog(parentComponent, "Conversion rate has invalid format. Try again.",
                        "Currency", JOptionPane.ERROR_MESSAGE);
                continue;
            }

            if (newCurrency.isPresent()) {
                var currency = newCurrency.get();

                ValidationInfo validationInfo = currency.validate();
                if (validationInfo.isInvalid())
                {
                    JOptionPane.showMessageDialog(parentComponent,
                            validationInfo.getMessage(),
                            "Currency", JOptionPane.ERROR_MESSAGE);
                    continue;
                }

                for (Currency curr : currencyRepository.findAll()) {
                    if (curr.getAbbreviation().equals(currency.getAbbreviation())) {
                        JOptionPane.showMessageDialog(parentComponent, "This currency is already in the application.",
                                "Currency", JOptionPane.ERROR_MESSAGE);
                        continue outerloop;
                    }
                }

                try {
                    Logger.debug("Adding currency :" + currency.detailedPrint());
                    currencyRepository.create(currency);
                    return;
                } catch (DataStorageException ex) {
                    Logger.error(ex);
                    JOptionPane.showMessageDialog(parentComponent,
                            "Unable to add such a currency. Make sure all parameters are valid.",
                            "Currency", JOptionPane.ERROR_MESSAGE);
                }
            } else {
                return;
            }
        }
    }

    private void deleteCurrency(ActionEvent e) {
        if (currencyModel.getSize() == 0) {
            JOptionPane.showMessageDialog(parentComponent, "There is no currency left!",
                    "Currency", JOptionPane.ERROR_MESSAGE);
        } else {
            var currency = (Currency) currencyComboBox.getSelectedItem();
            assert currency != null;  // should never be null, since Euro should be always present

            if (currency.getAbbreviation().equals("EUR")) {
                JOptionPane.showMessageDialog(parentComponent,
                        "It is not possible to remove currency Euro.",
                        "Currency", JOptionPane.ERROR_MESSAGE);
                setDialogItems();
                return;
            }
            Object[] optionsToDelete = {"Yes", "No"};
            var optionToDelete = JOptionPane.showOptionDialog(parentComponent,
                    "Do you really want to delete this currency?",
                    "Delete currency", JOptionPane.YES_NO_OPTION,
                    JOptionPane.QUESTION_MESSAGE, null, optionsToDelete, optionsToDelete[0]);
            if (optionToDelete == JOptionPane.NO_OPTION) {
                return;
            }
            var affectedTransactions = getAffectedTransactions(currency);
            if (!affectedTransactions.isEmpty()) {
                Object[] options = {"OK", "Cancel"};
                var option = JOptionPane.showOptionDialog(parentComponent,
                        "The currency you want to delete is used in at least one transaction. " +
                                "All affected transactions' currency will be changed to Euro " +
                                "and selected currency will be deleted afterwards.",
                        "Delete currency", JOptionPane.YES_NO_OPTION,
                        JOptionPane.QUESTION_MESSAGE, null, options, options[0]);
                if (option == JOptionPane.YES_OPTION) {
                    var euroCurrency = getEuroCurrency();
                    affectedTransactions.forEach(t -> changeTransactionsCurrencyToEuro(t, euroCurrency));
                } else {
                    return;
                }
            }
            try {
                Logger.debug("Deleting the currency: " + currency.detailedPrint());
                currencyRepository.delete(currency);
                currencyComboBox.setSelectedItem(currencyModel.getElementAt(0));
            } catch (DataStorageException ex) {
                Logger.error(ex);
                JOptionPane.showMessageDialog(parentComponent,
                        "Unable to delete this currency.",
                        "Currency", JOptionPane.ERROR_MESSAGE);
            }
        }
    }

    private List<Transaction> getAffectedTransactions(Currency currency) {
        return BusinessLogicImpl
                .getInstance()
                .getTransactionRepository()
                .findAll()
                .stream()
                .filter(t -> t.getAmount().getCurrency().equals(currency))
                .toList();
    }

    private Currency getEuroCurrency() {
        return BusinessLogicImpl
                .getInstance()
                .getCurrencyRepository()
                .getCurrency("EUR");
    }

    private void changeTransactionsCurrencyToEuro(Transaction transaction, Currency euroCurrency) {
        var amount = transaction
                .getAmount()
                .getAmount();
        var conversionRate = transaction
                .getAmount()
                .getCurrency()
                .getConversionRate();
        transaction.setAmount(new Amount(amount * conversionRate, euroCurrency));
        BusinessLogicImpl
                .getInstance()
                .editTransaction(transaction);
    }
}
