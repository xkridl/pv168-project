package cz.muni.fi.pv168.seminar02.team4.model;

import cz.muni.fi.pv168.seminar02.team4.model.enums.FileFormat;

import java.io.File;

/**
 * Class encapsulating all the necessary information where to export
 * the data, i.e. file, and how to export this data, e.g. in XML format.
 *
 * @author Adam Kridl
 */
public class ExportInfo {
    private final File exportTo;
    private final FileFormat exportFileFormat;

    public ExportInfo(File exportTo, FileFormat exportFormat) {
        this.exportTo = exportTo;
        this.exportFileFormat = exportFormat;
    }

    public File getExportTo() {
        return exportTo;
    }

    public FileFormat getExportForm() {
        return exportFileFormat;
    }
}
