package cz.muni.fi.pv168.seminar02.team4.ui.tabs;

import cz.muni.fi.pv168.seminar02.team4.logic.BusinessLogicImpl;
import cz.muni.fi.pv168.seminar02.team4.logic.FilterBL;
import cz.muni.fi.pv168.seminar02.team4.model.Category;
import cz.muni.fi.pv168.seminar02.team4.model.Currency;
import cz.muni.fi.pv168.seminar02.team4.model.enums.Period;
import cz.muni.fi.pv168.seminar02.team4.ui.model.CategoryListeningListModelDecorator;
import cz.muni.fi.pv168.seminar02.team4.ui.model.ComboBoxModelAdapter;
import cz.muni.fi.pv168.seminar02.team4.ui.model.CurrencyListeningListModelDecorator;
import cz.muni.fi.pv168.seminar02.team4.ui.model.ListeningListModel;
import cz.muni.fi.pv168.seminar02.team4.ui.model.ListeningListModelImpl;
import cz.muni.fi.pv168.seminar02.team4.ui.model.LocalDateModel;
import net.miginfocom.swing.MigLayout;
import org.jdatepicker.DateModel;
import org.jdatepicker.JDatePicker;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.time.LocalDate;
import java.util.Objects;

public class FilterGui {
    protected JPanel panel;
    private JCheckBox incomingCheckBox;
    private JCheckBox outgoingCheckBox;
    private final DateModel<LocalDate> fromDateModel = new LocalDateModel();
    private final DateModel<LocalDate> toDateModel = new LocalDateModel();
    private final CategoryListeningListModelDecorator categoryListModelDecorator;
    private final CurrencyListeningListModelDecorator currencyListModelDecorator;
    private JComboBox<Category> categoryComboBox;
    private JComboBox<Currency> currencyComboBox;
    protected JButton resetButton;
    private JCheckBox ordinaryCheckbox;
    private JCheckBox repeatingCheckbox;
    private JComboBox<Period> periodComboBox;
    private final BusinessLogicImpl BL = BusinessLogicImpl.getInstance();
    private final FilterBL filterBL;
    private LocalDate fromDateBackup;
    private LocalDate toDateBackup;
    private Component parentComponent;

    public FilterGui(Component parentComponent) {
        this.parentComponent = parentComponent;

        ListeningListModel<Category> categoryListModel = new ListeningListModelImpl<>();
        categoryListModelDecorator = new CategoryListeningListModelDecorator(categoryListModel);
        BusinessLogicImpl.getInstance().addCategoryListener(categoryListModelDecorator);

        ListeningListModel<Currency> currencyListModel = new ListeningListModelImpl<>();
        currencyListModelDecorator = new CurrencyListeningListModelDecorator(currencyListModel);
        BusinessLogicImpl.getInstance().addCurrencyListener(currencyListModelDecorator);

        addFilterItems();
        this.filterBL = new FilterBL();
        triggerFilterChange(null);
    }

    private void addFilterItems() {
        panel = new JPanel(new MigLayout("gap 20px 5px, flowy, wrap 2"));
        incomingCheckBox = new JCheckBox("Incoming");
        incomingCheckBox.setSelected(true);
        incomingCheckBox.addActionListener(this::triggerFilterChange);
        panel.add(incomingCheckBox);

        outgoingCheckBox = new JCheckBox("Outgoing");
        outgoingCheckBox.setSelected(true);
        outgoingCheckBox.addActionListener(this::triggerFilterChange);
        panel.add(outgoingCheckBox);

        JLabel fromLabel = new JLabel("From");
        panel.add(fromLabel);
        var fromJDatePicker = new JDatePicker(fromDateModel);
        fromJDatePicker.addActionListener(e -> {
            if (!isDateValid(fromDateModel, toDateModel)) {
                fromDateModel.setValue(fromDateBackup);
            } else {
                fromDateBackup = fromDateModel.getValue();
            }
            triggerFilterChange(e);
        });
        panel.add(fromJDatePicker);

        JLabel toLabel = new JLabel("To");
        panel.add(toLabel);
        var toJDatePicker = new JDatePicker(toDateModel);
        toJDatePicker.addActionListener(e -> {
            if (!isDateValid(fromDateModel, toDateModel)) {
                toDateModel.setValue(toDateBackup);
            } else {
                toDateBackup = toDateModel.getValue();
            }
            triggerFilterChange(e);
        });
        panel.add(toJDatePicker);

        JLabel categoryLabel = new JLabel("Category");
        panel.add(categoryLabel);
        categoryComboBox = new JComboBox<>(new ComboBoxModelAdapter<>(categoryListModelDecorator));
        categoryComboBox.setSelectedIndex(0);
        categoryComboBox.addActionListener(this::triggerFilterChange);
        panel.add(categoryComboBox);

        JLabel currencyLabel = new JLabel("Currency");
        panel.add(currencyLabel);
        currencyComboBox = new JComboBox<>(new ComboBoxModelAdapter<>(currencyListModelDecorator));
        currencyComboBox.setSelectedIndex(0);
        currencyComboBox.addActionListener(this::triggerFilterChange);
        panel.add(currencyComboBox);

        ordinaryCheckbox = new JCheckBox("Ordinary", true);
        ordinaryCheckbox.addActionListener(this::triggerFilterChange);
        panel.add(ordinaryCheckbox);
        repeatingCheckbox = new JCheckBox("Repeating", true);
        repeatingCheckbox.addActionListener(this::triggerFilterChange);
        repeatingCheckbox.addActionListener(this::updatePeriodVisibility);
        panel.add(repeatingCheckbox);

        JLabel periodLabel = new JLabel("Period");
        panel.add(periodLabel);
        periodComboBox = new JComboBox<>(Period.values());
        periodComboBox.addActionListener(this::triggerFilterChange);
        panel.add(periodComboBox);

        resetButton = new JButton("Reset");
        resetButton.addActionListener(this::resetFilter);
        panel.add(new JLabel());
        panel.add(resetButton);
    }

    private Boolean isDateValid(DateModel<LocalDate> from, DateModel<LocalDate> to) {
        if (from.getValue() != null && to.getValue() != null && to.getValue().isBefore(from.getValue())) {
            JOptionPane.showMessageDialog(parentComponent,
                    "Entered date range is unreal.",
                    "Wrong date", JOptionPane.ERROR_MESSAGE);
            return false;
        }
        return true;
    }

    private void triggerFilterChange(ActionEvent e) {
        this.filterBL.setIncoming(incomingCheckBox.isSelected());
        this.filterBL.setOutgoing(outgoingCheckBox.isSelected());
        this.filterBL.setFromDate(fromDateModel.getValue());
        this.filterBL.setToDate(toDateModel.getValue());
        this.filterBL.setCategory(Objects.requireNonNull(categoryComboBox.getSelectedItem()).toString());
        this.filterBL.setCurrency(Objects.requireNonNull(currencyComboBox.getSelectedItem()).toString());
        this.filterBL.setOrdinary(ordinaryCheckbox.isSelected());
        this.filterBL.setRepeating(repeatingCheckbox.isSelected());
        this.filterBL.setPeriod(Objects.requireNonNull(periodComboBox.getSelectedItem()).toString());
        BL.updateTransactionFilter(this.filterBL);
    }

    protected void resetFilter(ActionEvent e) {
        incomingCheckBox.setSelected(true);
        outgoingCheckBox.setSelected(true);
        categoryComboBox.setSelectedIndex(0);
        currencyComboBox.setSelectedIndex(0);
        fromDateModel.setValue(null);
        toDateModel.setValue(null);
        ordinaryCheckbox.setSelected(true);
        repeatingCheckbox.setSelected(true);
        periodComboBox.setSelectedIndex(0);
        triggerFilterChange(e);
    }

    public JPanel getPanel() {
        return panel;
    }

    private void updatePeriodVisibility(ActionEvent e) {
        var checkbox = (JCheckBox) e.getSource();
        periodComboBox.setEnabled(checkbox.isSelected());
    }
}
