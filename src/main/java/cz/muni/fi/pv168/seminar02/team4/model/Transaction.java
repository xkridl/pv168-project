package cz.muni.fi.pv168.seminar02.team4.model;

import cz.muni.fi.pv168.seminar02.team4.model.enums.Period;
import cz.muni.fi.pv168.seminar02.team4.model.enums.TransactionType;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class Transaction {
    private Long id;
    private String name;
    private String description;
    private Amount amount;
    private LocalDate date;
    private Category category;
    private TransactionType transactionType;
    private Period period;

    public Transaction(
            Long id,
            String name,
            String description,
            Amount amount,
            LocalDate date,
            Category category,
            TransactionType transactionType,
            Period period) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.amount = amount;
        this.category = category;
        this.date = date;
        this.transactionType = transactionType;
        this.period = period;
    }

    public Transaction(
            String name,
            String description,
            Amount amount,
            LocalDate date,
            Category category,
            TransactionType transactionType,
            Period period) {
        this(null, name, description, amount, date, category, transactionType, period);
    }

    public Transaction(
            String name,
            String description,
            Amount amount,
            LocalDate date,
            Category category,
            TransactionType transactionType) {
        this(name, description, amount, date, category, transactionType, null);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = Objects.requireNonNull(id);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Amount getAmount() {
        return amount;
    }

    public Double getAmountValue() {return amount.getAmount();}

    public void setAmount(Amount amount) {
        this.amount = amount;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public void setTransactionType(TransactionType transactionType) {
        this.transactionType = transactionType;
    }

    public TransactionType getTransactionType() {
        return transactionType;
    }

    public Period getPeriod() {
        return period;
    }

    public void setPeriod(Period period) {
        this.period = period;
    }

    public String printPeriodInfo() {
        if (!isRepeating()) {
            return "";
        }
        return period.toString();
    }

    public String printAmount() {
        var amountValue = amount.getAmount();
        if (isOutgoing()) {
            amountValue *= -1;
        }
        return String.format("%.2f", amountValue) + " " + amount.getCurrency();
    }
    public Transaction createSnapshot() {
        return new Transaction(getName(), getDescription(), getAmount(), getDate(), getCategory(), getTransactionType(), getPeriod());
    }

    public void setAttributes(Transaction transaction) {
        setName(transaction.getName());
        setDescription(transaction.getDescription());
        setAmount(transaction.getAmount());
        setDate(transaction.getDate());
        setCategory(transaction.getCategory());
        setTransactionType(transaction.getTransactionType());
        setPeriod(transaction.getPeriod());
    }

    public ValidationInfo validate() {
        if (hasEmptyName()) {
            return new ValidationInfo(true, "Transaction name is required.");
        }
        if (getName().length() > 50) {
            return new ValidationInfo(true, "Transaction name cannot be longer than 50 characters.");
        }
        if (getDescription().length() > 200) {
            return new ValidationInfo(true, "Transaction description cannot be longer than 200 characters.");
        }
        if (hasZeroAmount()) {
            return new ValidationInfo(true, "Amount must not be zero.");
        }
        if (hasUnfilledDate()) {
            return new ValidationInfo(true, "Transaction Date is required.");
        }
        return new ValidationInfo(false, null);
    }
    public boolean isOrdinary() {
        return period == null;
    }

    public boolean isRepeating() {
        return period != null;
    }

    public boolean isOutgoing() {
        return transactionType == TransactionType.OUTGOING;
    }

    public boolean isIncoming() {
        return transactionType == TransactionType.INCOMING;
    }

    @Override
    public String toString() {
        return "Transaction{" +
                "name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", amount=" + amount +
                ", date=" + date +
                ", category=" + category +
                ", transaction type=" + transactionType +
                ", period=" + period +
                '}';
    }

    public String detailedPrint() {
        return "Transaction={"
                + "name: " + name
                + ", description: " + description
                + ", amount: " + amount
                + ", date: " + date
                + ", category: " + category.detailedPrint()
                + ", transaction type: " + transactionType
                + ", period: " + period
                + "}";
    }

    public String toCSV() {
        return "\"" + name + "\",\""
                + description + "\","
                + amount.getAmount() + " " + amount.getCurrency().getAbbreviation() + ","  // e.g. 34 EUR,
                + date + ",\""
                + category + "\","
                + transactionType + ","
                + (period != null ? period.toString() : "");
    }

    public Map<String, String> toJSON() {
        Map<String,String> transactionAsJsonObject = new HashMap<>();
        transactionAsJsonObject.put("Name", name);
        transactionAsJsonObject.put("Description", description);
        transactionAsJsonObject.put("Amount",  amount.getAmount() + " " + amount.getCurrency().getAbbreviation());
        transactionAsJsonObject.put("Date", date.toString());
        transactionAsJsonObject.put("Category", category.toString());
        transactionAsJsonObject.put("Type", transactionType.toString());
        transactionAsJsonObject.put("Period", period != null ? period.toString() : "");
        return transactionAsJsonObject;
    }

    public Element toXML(Document doc) {
        Element transactionElement = doc.createElement("transaction");

        Element nameElement = doc.createElement("name");
        nameElement.setTextContent(name);
        transactionElement.appendChild(nameElement);

        Element descriptionElement = doc.createElement("description");
        descriptionElement.setTextContent(description);
        transactionElement.appendChild(descriptionElement);

        Element amountElement = doc.createElement("amount");
        amountElement.setTextContent(amount.getAmount() + " " + amount.getCurrency().getAbbreviation());
        transactionElement.appendChild(amountElement);

        Element dateElement = doc.createElement("date");
        dateElement.setTextContent(date.toString());
        transactionElement.appendChild(dateElement);

        Element categoryElement = doc.createElement("category");
        categoryElement.setTextContent(category.toString());
        transactionElement.appendChild(categoryElement);

        Element typeElement = doc.createElement("type");
        typeElement.setTextContent(transactionType.toString());
        transactionElement.appendChild(typeElement);

        Element periodElement = doc.createElement("period");
        periodElement.setTextContent(period != null ? period.toString() : "");
        transactionElement.appendChild(periodElement);

        return transactionElement;
    }

    public boolean hasEmptyName() {
        return getName().isEmpty();
    }

    public boolean hasZeroAmount() {
        return getAmount().getAmount().compareTo(0.0d) == 0;
    }

    public boolean hasNegativeAmount() {
        return getAmount().getAmount().compareTo(0.0d) <= 0;
    }

    public boolean hasUnfilledDate() {
        return getDate() == null;
    }
}
