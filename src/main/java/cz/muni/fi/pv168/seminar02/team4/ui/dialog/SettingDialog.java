package cz.muni.fi.pv168.seminar02.team4.ui.dialog;

import com.formdev.flatlaf.FlatDarkLaf;
import com.formdev.flatlaf.FlatLightLaf;
import cz.muni.fi.pv168.seminar02.team4.logic.BusinessLogicImpl;
import cz.muni.fi.pv168.seminar02.team4.model.Category;
import cz.muni.fi.pv168.seminar02.team4.model.enums.Theme;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.util.Objects;

public class SettingDialog extends Dialog {

    private final Component parentComponent;

    private JComboBox<Theme> themeComboBox;

    public SettingDialog(String constraints, String title, Component parentComponent) {
        super(constraints, title, parentComponent );
        this.parentComponent = parentComponent;
    }

    @Override
    public void addDialogItems() {
        var themeLabel = new JLabel("Theme");
        themeComboBox = new JComboBox<>(Theme.values());
        themeComboBox.setSelectedItem(BusinessLogicImpl.getInstance().getCurrentTheme());
        themeComboBox.addActionListener(this::changeTheme);
        panel.add(themeLabel);
        panel.add(themeComboBox);
    }

    private void changeTheme(ActionEvent e) {
        Category noCategory = BusinessLogicImpl.getInstance().getCategoryRepository().findCategory("No category");
        Theme selectedTheme = (Theme) themeComboBox.getSelectedItem();
        switch (Objects.requireNonNull(selectedTheme)) {
            case FLAT_LAF_DARK -> {
                noCategory.setColor(new Color(70, 73, 75));
                BusinessLogicImpl.getInstance().setTextFieldMainColor(new Color(70, 73, 75));
                FlatDarkLaf.setup();
            }
            case FLAT_LAF_LIGHT -> {
                noCategory.setColor(Color.white);
                BusinessLogicImpl.getInstance().setTextFieldMainColor(Color.white);
                FlatLightLaf.setup();
            }
        }


        BusinessLogicImpl.getInstance().setCurrentTheme(selectedTheme);
        //parentComponent == main JFrame
        SwingUtilities.updateComponentTreeUI(parentComponent);
    }
}
