package cz.muni.fi.pv168.seminar02.team4.ui.model;

import cz.muni.fi.pv168.seminar02.team4.model.Category;
import cz.muni.fi.pv168.seminar02.team4.model.Transaction;

import java.time.LocalDate;
import java.util.List;

public class TransactionTableModel extends EntityTableModel<Transaction> {
    public TransactionTableModel() {
        super(List.of(
                Column.readonly("Date", LocalDate.class, Transaction::getDate),
                Column.readonly("Amount", String.class, Transaction::printAmount),
                Column.editable("Category", Category.class, Transaction::getCategory, Transaction::setCategory),
                Column.readonly("Name", String.class, Transaction::getName),
                Column.readonly("Period", String.class, Transaction::printPeriodInfo),
                Column.editable("Description", String.class, Transaction::getDescription, Transaction::setDescription)
        ));
    }
}
