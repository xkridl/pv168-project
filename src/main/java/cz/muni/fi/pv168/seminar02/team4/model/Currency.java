package cz.muni.fi.pv168.seminar02.team4.model;

import java.util.Objects;

public class Currency {
    private Long id;
    private String name;
    private String abbreviation;
    private Double conversionRate;

    public Currency(Long id, String name, String abbreviation, Double conversionRate) {
        this.id = id;
        this.name = name;
        this.abbreviation = abbreviation;
        this.conversionRate = conversionRate;
    }

    public Currency(String name, String abbreviation, Double conversionRate) {
        this(null, name, abbreviation, conversionRate);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = Objects.requireNonNull(id);
    }

    public String getName() {
        return name;
    }

    public String getAbbreviation() {
        return abbreviation;
    }

    public Double getConversionRate() {
        return conversionRate;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAbbreviation(String abbreviation) {
        this.abbreviation = abbreviation;
    }

    public void setConversionRate(Double conversionRate) {
        this.conversionRate = conversionRate;
    }

    @Override
    public String toString() {
        return abbreviation;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || obj.getClass() != this.getClass()) {
            return false;
        }
        Currency currency2 = (Currency) obj;
        return currency2.abbreviation.equals(this.abbreviation);
    }

    @Override
    public int hashCode() {
        return abbreviation.hashCode();
    }

    public String detailedPrint() {
        return "Currency={"
                + "name: " + name
                + ", abbreviation: " + abbreviation
                + ", conversion rate: " + conversionRate
                + "}";
    }

    public Currency createSnapshot() {
        return new Currency(getName(), getAbbreviation(), getConversionRate());
    }

    public void setAttributes(Currency currency) {
        setName(currency.getName());
        setAbbreviation(currency.getAbbreviation());
        setConversionRate(currency.getConversionRate());
    }

    public ValidationInfo validate() {
        if (getName().isEmpty()) {
            return new ValidationInfo(true, "Currency name is required.");
        }
        if (getAbbreviation().isEmpty()) {
            return new ValidationInfo(true, "Currency abbreviation is required.");
        }
        if (getAbbreviation().length() > 3) {
            return new ValidationInfo(true, "Currency abbreviation cannot be longer than 3 characters.");
        }
        if (getName().length() > 50) {
            return new ValidationInfo(true, "Currency name cannot be longer than 50 characters.");
        }
        return new ValidationInfo(false, null);
    }
}
