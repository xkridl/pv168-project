package cz.muni.fi.pv168.seminar02.team4.data.manipulation;

import cz.muni.fi.pv168.seminar02.team4.model.Transaction;

import javax.swing.JOptionPane;
import java.awt.*;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Collection;

class CsvTransactionExporter implements TransactionExporter {
    private final Component parentComponent;
    public CsvTransactionExporter(Component parentComponent) {
        this.parentComponent = parentComponent;
    }

    @Override
    public void exportTransactions(Collection<Transaction> transactions, String filePath) {
        try (var writer = new BufferedWriter(new FileWriter(filePath, StandardCharsets.UTF_8))) {
            for (var transaction : transactions) {
                writer.write(transaction.toCSV());
                writer.newLine();
            }
            // Uncomment the following line to simulate blocking export operation
            // Thread.sleep(10_000);
        } catch (IOException exception) {
            JOptionPane.showMessageDialog(parentComponent,
                    "The application was unable to write to that file.",
                    "Export", JOptionPane.ERROR_MESSAGE);
        }
    }
}
