package cz.muni.fi.pv168.seminar02.team4.ui.dialog;

import cz.muni.fi.pv168.seminar02.team4.model.Category;

import javax.swing.JColorChooser;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.colorchooser.AbstractColorChooserPanel;
import javax.swing.colorchooser.DefaultColorSelectionModel;
import java.awt.Color;
import java.util.Objects;

/**
 * Dialog for adding new category.
 */
public class CategoryDialog extends EntityDialog<Category> {
    private final JTextField nameField;
    private final JColorChooser colorChooser;
    private final Category category;

    public CategoryDialog(Category category, String constraints, String title) {
        super(constraints, title);
        this.category = initializeCategory(category);

        nameField = new JTextField();
        colorChooser = createColorChooser();

        setDialogItemValues();
        addDialogItems();
    }

    public CategoryDialog(String constraints, String title) {
        this(null, constraints, title);
    }

    public CategoryDialog(String title) {
        this("", title);
    }

    public CategoryDialog(Category category, String title) {
        this(category, "", title);
    }

    private Category initializeCategory(Category category) {
        return Objects.requireNonNullElseGet(category, () -> new Category("", Color.WHITE));
    }

    private JColorChooser createColorChooser() {
        var colorSelectionModel = new DefaultColorSelectionModel();
        var colorChooser = new JColorChooser(colorSelectionModel);
        var emptyPanel = new JPanel();

        colorChooser.setPreviewPanel(emptyPanel);
        var swatchesPanel = colorChooser.getChooserPanels()[0];
        colorChooser.setChooserPanels(new AbstractColorChooserPanel[]{swatchesPanel});

        return colorChooser;
    }

    void setDialogItemValues() {
        nameField.setText(category.getName());
        colorChooser.setColor(category.getColor());
    }

    @Override
    public void addDialogItems() {
        add("Name", nameField, true, "wrap, gap 5px, w 90%");
        add("Color", colorChooser, false, "wrap, grow");
    }

    @Override
    Category getEntity() {
        category.setName(nameField.getText());
        category.setColor(colorChooser.getColor());
        return category;
    }

}
