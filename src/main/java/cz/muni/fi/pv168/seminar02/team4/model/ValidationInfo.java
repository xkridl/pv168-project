package cz.muni.fi.pv168.seminar02.team4.model;

public class ValidationInfo {
    private final boolean isInvalid;
    private final String message;

    ValidationInfo(boolean isInvalid, String message) {
        this.isInvalid = isInvalid;
        this.message = message;
    }

    public boolean isInvalid() {
        return isInvalid;
    }

    public String getMessage() {
        return message;
    }



}
