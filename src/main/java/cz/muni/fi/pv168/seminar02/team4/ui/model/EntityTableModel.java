package cz.muni.fi.pv168.seminar02.team4.ui.model;

import cz.muni.fi.pv168.seminar02.team4.logic.Listener;

import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableModel;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Generic table model.
 *
 * @param <E> - type of entities in the table model
 * @author Adam Kridl
 */
public class EntityTableModel<E> extends AbstractTableModel implements TableModel, Listener<E> {
    private List<E> entities;
    private final List<Column<E, ?>> columns;

    public EntityTableModel(List<Column<E, ?>> columns) {
        this.entities = new ArrayList<>();
        this.columns = columns;
    }

    @Override
    public int getRowCount() {
        return entities.size();
    }

    @Override
    public int getColumnCount() {
        return columns.size();
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        var entity = getEntityAtIndex(rowIndex);
        return columns.get(columnIndex).getValue(entity);
    }

    @Override
    public String getColumnName(int column) {
        return columns.get(column).getName();
    }

    public E getEntityAtIndex(int rowIndex) {
        if (rowIndex < 0 || rowIndex >= getRowCount()) {
            throw new IndexOutOfBoundsException("Invalid entity index.");
        }
        return entities.get(rowIndex);
    }

    @Override
    public void notify(Collection<E> data) {
        entities = (List<E>) data;
        fireTableDataChanged();
    }
}
