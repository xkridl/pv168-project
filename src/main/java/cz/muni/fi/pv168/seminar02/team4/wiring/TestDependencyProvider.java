package cz.muni.fi.pv168.seminar02.team4.wiring;

import cz.muni.fi.pv168.seminar02.team4.data.storage.db.DatabaseManager;

public class TestDependencyProvider extends CommonDependencyProvider {
    public TestDependencyProvider() {
        super(getDatabaseManager());
    }
    private static DatabaseManager getDatabaseManager() {
      return DatabaseManager.createTestInstance();
    }
}
