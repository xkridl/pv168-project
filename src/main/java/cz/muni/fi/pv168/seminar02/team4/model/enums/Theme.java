package cz.muni.fi.pv168.seminar02.team4.model.enums;

public enum Theme {
    FLAT_LAF_LIGHT,
    FLAT_LAF_DARK;

    @Override
    public String toString() {
        return switch (this) {
            case FLAT_LAF_LIGHT -> "Light";
            case FLAT_LAF_DARK -> "Dark";
        };
    }
}
