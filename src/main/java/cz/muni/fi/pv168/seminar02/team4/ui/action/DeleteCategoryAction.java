package cz.muni.fi.pv168.seminar02.team4.ui.action;

import cz.muni.fi.pv168.seminar02.team4.data.storage.DataStorageException;
import cz.muni.fi.pv168.seminar02.team4.logic.BusinessLogicImpl;
import cz.muni.fi.pv168.seminar02.team4.model.Category;
import cz.muni.fi.pv168.seminar02.team4.model.Transaction;
import cz.muni.fi.pv168.seminar02.team4.ui.model.CategoryTableModel;
import cz.muni.fi.pv168.seminar02.team4.ui.resources.Icons;
import org.tinylog.Logger;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.KeyStroke;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class DeleteCategoryAction extends AbstractAction implements Action {
    private final JTable categoryTable;
    private final Component parentComponent;

    public DeleteCategoryAction(Component parentComponent, JTable categoryTable) {
        super("Delete", Icons.DELETE_CATEGORY_ICON);
        this.categoryTable = categoryTable;
        this.parentComponent = parentComponent;

        putValue(SHORT_DESCRIPTION, "Deletes selected transaction/-s");
        putValue(MNEMONIC_KEY, KeyEvent.VK_D);
        putValue(ACCELERATOR_KEY, KeyStroke.getKeyStroke("DELETE"));
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        var categoryTableModel = (CategoryTableModel) categoryTable.getModel();
        var categoriesToDelete = Arrays.stream(categoryTable.getSelectedRows())
                .mapToObj(categoryTableModel::getEntityAtIndex)
                .toList();

        if (categoriesToDelete.size() == 1 && categoriesToDelete.get(0).getName().equals("No category")) {
            JOptionPane.showMessageDialog(parentComponent,
                    "No category cannot be deleted",
                    "Delete Categories", JOptionPane.ERROR_MESSAGE);
            return;
        }

        if (categoriesToDelete.size() > 1) {
            Object[] options = {"Yes", "No"};
            var option = JOptionPane.showOptionDialog(parentComponent,
                    "Multiple categories are selected. " +
                            "Do you really want to delete all selected categories?",
                    "Delete categories", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE,
                    null, options, options[1]);
            if (option != JOptionPane.YES_OPTION) {
                return;
            }
        }


        for (var category : categoriesToDelete) {
            if (category.getName().equals("No category")) {
                Object[] options = {"Delete others", "Cancel"};
                var option = JOptionPane.showOptionDialog(parentComponent,
                        "'No category' cannot be deleted. " +
                                "You can delete other selected categories, or cancel this operation.",
                        "Delete Categories", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE,
                        null, options, options[0]);

                if (option == JOptionPane.YES_OPTION) {
                    categoriesToDelete = categoriesToDelete
                            .stream()
                            .filter(c -> !c.getName().equals("No category"))
                            .collect(Collectors.toList());
                } else {
                    return;
                }
            }
        }

        var affectedTransactions = getAffectedTransactions(categoriesToDelete);
        if (!affectedTransactions.isEmpty()) {
            Object[] options = {"OK", "Cancel"};
            var option = JOptionPane.showOptionDialog(parentComponent,
            "The category you want to delete is used in at least one transaction. " +
                    "All categories used in any transaction will be changed to 'No category' " +
                    "and deleted afterwards.", "Delete category", JOptionPane.YES_NO_OPTION,
                    JOptionPane.QUESTION_MESSAGE, null, options, options[0]);
            if (option == JOptionPane.YES_OPTION) {
                var noCategory = getNoCategory();
                affectedTransactions.forEach(t -> changeTransactionsCategoryToNoCategory(t, noCategory));
            } else {
                return;
            }
        }

        try {
            Logger.debug("Going to delete the following categories:");
            categoriesToDelete.forEach(category -> Logger.debug(category.detailedPrint()));
            BusinessLogicImpl.getInstance().deleteCategories(categoriesToDelete);
            Logger.debug("All categories were successfully deleted");
        } catch (DataStorageException ex) {
            Logger.error(ex);
        }
    }

    private Category getNoCategory() {
        return BusinessLogicImpl
                .getInstance()
                .getCategoryRepository()
                .findCategory("No category");
    }
    private List<Transaction> getAffectedTransactions(List<Category> categoriesToDelete) {
        var allTransactions = BusinessLogicImpl
                .getInstance()
                .getTransactionRepository()
                .findAll();
        List<Transaction> affectedTransactions = new ArrayList<>();
        for (var transaction : allTransactions) {
            for (var category : categoriesToDelete) {
                if (transaction.getCategory().equals(category)) {
                    affectedTransactions.add(transaction);
                    break;
                }
            }
        }
        return affectedTransactions;
    }

    private void changeTransactionsCategoryToNoCategory(Transaction transaction, Category noCategory) {
        transaction.setCategory(noCategory);
        BusinessLogicImpl.getInstance().editTransaction(transaction);
    }
}
