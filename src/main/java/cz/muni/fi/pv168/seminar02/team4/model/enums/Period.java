package cz.muni.fi.pv168.seminar02.team4.model.enums;

public enum Period {
    ANY,
    ONE_DAY,
    THREE_DAYS,
    ONE_WEEK,
    TWO_WEEKS,
    FOUR_WEEKS,
    ONE_YEAR;

    @Override
    public String toString() {
        return switch (this) {
            case ANY -> "Any";
            case ONE_DAY -> "1 day";
            case THREE_DAYS -> "3 days";
            case ONE_WEEK -> "1 week";
            case TWO_WEEKS -> "2 weeks";
            case FOUR_WEEKS -> "4 weeks";
            case ONE_YEAR -> "1 year";
        };
    }

    public static Period parse(String input) {
        return switch (input) {
            case "Any" -> ANY;
            case "1 day" -> ONE_DAY;
            case "3 days" -> THREE_DAYS;
            case "1 week" -> ONE_WEEK;
            case "2 weeks" -> TWO_WEEKS;
            case "4 weeks" -> FOUR_WEEKS;
            case "1 year" -> ONE_YEAR;
            default -> null;
        };
    }

    public int toDays() {
        return switch (this) {
            case ONE_DAY -> 1;
            case THREE_DAYS -> 3;
            case ONE_WEEK -> 7;
            case TWO_WEEKS -> 14;
            case FOUR_WEEKS -> 28;
            case ONE_YEAR -> 365;
            default -> 0;
        };
    }
}
