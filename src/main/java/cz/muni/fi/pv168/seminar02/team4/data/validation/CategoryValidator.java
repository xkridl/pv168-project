package cz.muni.fi.pv168.seminar02.team4.data.validation;

import cz.muni.fi.pv168.seminar02.team4.data.validation.common.StringLengthValidator;
import cz.muni.fi.pv168.seminar02.team4.model.Category;

import java.util.List;

/**
 * Validator of {@link Category} class.
 */
public class CategoryValidator implements Validator<Category> {
    @Override
    public ValidationResult validate(Category model) {
        var validators = List.of(
                Validator.extracting(
                        Category::getName, new StringLengthValidator(1, 50, "Category name"))
        );

        return Validator.compose(validators).validate(model);
    }
}
