package cz.muni.fi.pv168.seminar02.team4.ui.resources;

import javax.swing.*;
import java.net.URL;

public class Icons {
    public static final Icon ADD_ICON = createIcon("addIcon.png");
    public static Icon EDIT_ICON = createIcon("editIcon.png");
    public static final Icon ADD_CATEGORY_ICON = createIcon("addCategoryIcon.png");
    public static final Icon ADD_TRANSACTION_ICON = createIcon("addTransactionIcon.png");
    public static final Icon DELETE_CATEGORY_ICON = createIcon("deleteCategoryIcon.png");
    public static final Icon DELETE_TRANSACTION_ICON = createIcon("deleteTransactionIcon.png");
    public static final Icon DELETE_ICON = createIcon("deleteIcon.png");
    public static final Icon SAVE_ICON = createIcon("saveIcon.png");

    private Icons() {
        throw new AssertionError("This class is not instantiable");
    }

    private static ImageIcon createIcon(String name) {
        URL url = Icons.class.getResource(name);
        if (url == null) {
            throw new IllegalArgumentException("Icon resource not found on classpath: " + name);
        }
        return new ImageIcon(url);
    }
}
