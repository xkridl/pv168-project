package cz.muni.fi.pv168.seminar02.team4.data.manipulation;

/**
 * Exception thrown if there is some problem with data import/export.
 *
 * @author Jiri Koudelka
 */
final class DataManipulationException extends RuntimeException {

    DataManipulationException(String message, Throwable cause) {
        super(message, cause);
    }
}
