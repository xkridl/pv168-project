package cz.muni.fi.pv168.seminar02.team4.data.storage.repository;

import cz.muni.fi.pv168.seminar02.team4.data.storage.dao.DataAccessObject;
import cz.muni.fi.pv168.seminar02.team4.data.storage.entity.TransactionEntity;
import cz.muni.fi.pv168.seminar02.team4.data.storage.mapper.EntityMapper;
import cz.muni.fi.pv168.seminar02.team4.logic.FilterBL;
import cz.muni.fi.pv168.seminar02.team4.logic.FilterablePublisher;
import cz.muni.fi.pv168.seminar02.team4.logic.Listener;
import cz.muni.fi.pv168.seminar02.team4.model.Transaction;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Repository of {@link Transaction} class.
 */
public class TransactionRepository implements Repository<Transaction>, FilterablePublisher<Transaction> {
    private final DataAccessObject<TransactionEntity> transactionDao;
    private final EntityMapper<TransactionEntity, Transaction> transactionMapper;
    private List<Transaction> transactions;
    private FilterBL filterBL;
    private final List<Listener<Transaction>> listenersOfFiltered;
    private final List<Listener<Transaction>> listenersOfAll;

    public TransactionRepository(
            DataAccessObject<TransactionEntity> transactionDao,
            EntityMapper<TransactionEntity, Transaction> transactionMapper
    ) {
        this.transactionDao = transactionDao;
        this.transactionMapper = transactionMapper;
        filterBL = new FilterBL();
        listenersOfFiltered = new ArrayList<>();
        listenersOfAll = new ArrayList<>();
        refresh();
    }

    public void setFilterBL(FilterBL filterBL) {
        this.filterBL = filterBL;
        notifyListenersOfFiltered(getMatchingEntities());
    }

    @Override
    public int getSize() {
        return transactions.size();
    }

    @Override
    public Optional<Transaction> findById(long id) {
        return findAll().stream()
                .filter(transaction -> transaction.getId() == id)
                .findFirst();
    }

    @Override
    public Optional<Transaction> findByIndex(int index) {
        if (0 <= index && index < getSize()) {
            return Optional.of(transactions.get(index));
        }
        return Optional.empty();
    }

    @Override
    public List<Transaction> findAll() {
        return Collections.unmodifiableList(transactions);
    }

    public Collection<Transaction> getMatchingEntities() {
        return findAll().stream()
                .filter(transaction -> filterBL.include(transaction))
                .toList();
    }

    @Override
    public void refresh() {
        transactions = fetchAll();
    }

    @Override
    public void create(Transaction transaction) {
        Stream.of(transaction)
                .map(transactionMapper::mapToEntity)
                .map(transactionDao::create)
                .map(transactionMapper::mapToModel)
                .forEach(transactions::add);
        notifyListeners();
    }

    @Override
    public void update(Transaction transaction) {
        int index = transactions.indexOf(transaction);
        Stream.of(transaction)
                .map(transactionMapper::mapToEntity)
                .map(transactionDao::update)
                .map(transactionMapper::mapToModel)
                .forEach(t -> transactions.set(index, t));
        notifyListeners();
    }

    @Override
    public void deleteByIndex(int index) {
        var transaction = transactions.get(index);
        transactionDao.deleteById(transaction.getId());
        transactions.remove(transaction);
        notifyListeners();
    }

    @Override
    public void delete(Transaction transaction) {
        deleteByIndex(transactions.indexOf(transaction));
    }

    private List<Transaction> fetchAll() {
        return transactionDao.findAll()
                .stream()
                .map(transactionMapper::mapToModel)
                .collect(Collectors.toList());
    }

    private void notifyListeners() {
        notifyListenersOfFiltered(getMatchingEntities());
        notifyListenersOfAll(findAll());
    }

    @Override
    public void notifyListenersOfFiltered(Collection<Transaction> data) {
        for (var listener : listenersOfFiltered) {
            listener.notify(getMatchingEntities());
        }
    }

    @Override
    public void notifyListenersOfAll(Collection<Transaction> data) {
        for (var listener : listenersOfAll) {
            listener.notify(data);
        }
    }

    @Override
    public void addListenerOfFiltered(Listener<Transaction> listener) {
        listenersOfFiltered.add(listener);
        listener.notify(getMatchingEntities());
    }

    @Override
    public void addListenerOfAll(Listener<Transaction> listener) {
        listenersOfAll.add(listener);
        listener.notify(findAll());
    }

    @Override
    public void removeListenerOfFiltered(Listener<Transaction> listener) {
        listenersOfFiltered.remove(listener);
    }

    @Override
    public void removeListenerOfAll(Listener<Transaction> listener) {
        listenersOfAll.remove(listener);
    }

    public void recomputeBalance() {
        notifyListeners();
    }
}
