package cz.muni.fi.pv168.seminar02.team4.logic;

import cz.muni.fi.pv168.seminar02.team4.model.Category;
import cz.muni.fi.pv168.seminar02.team4.model.ExportInfo;
import cz.muni.fi.pv168.seminar02.team4.model.ImportInfo;
import cz.muni.fi.pv168.seminar02.team4.model.Transaction;

import java.util.Collection;

/**
 * Main operations, which should the application provide.
 *
 * @author Adam Kridl
 */
public interface BusinessLogic {
    void addCategory(Category category);
    void editCategory(Category category);
    void deleteCategories(Collection<Category> categories);

    void addTransaction(Transaction transaction);
    void editTransaction(Transaction transaction);
    void deleteTransactions(Collection<Transaction> transactions);

    void doExport(ExportInfo exportInfo);
    void doImport(ImportInfo importInfo);

    void updateTransactionFilter(FilterBL filterBL);
}
