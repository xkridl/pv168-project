package cz.muni.fi.pv168.seminar02.team4.ui.action;

import cz.muni.fi.pv168.seminar02.team4.data.storage.DataStorageException;
import cz.muni.fi.pv168.seminar02.team4.logic.BusinessLogicImpl;
import cz.muni.fi.pv168.seminar02.team4.model.Category;
import cz.muni.fi.pv168.seminar02.team4.model.ValidationInfo;
import cz.muni.fi.pv168.seminar02.team4.ui.dialog.CategoryDialog;
import cz.muni.fi.pv168.seminar02.team4.ui.model.CategoryTableModel;
import cz.muni.fi.pv168.seminar02.team4.ui.resources.Icons;
import org.tinylog.Logger;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.KeyStroke;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.util.Optional;

public class EditCategoryAction extends AbstractAction implements Action {
    private final Component parentComponent;
    private final JTable categoryTable;

    public EditCategoryAction(Component parentComponent, JTable categoryTable) {
        super("Edit", Icons.EDIT_ICON);
        this.parentComponent = parentComponent;
        this.categoryTable = categoryTable;

        putValue(SHORT_DESCRIPTION, "Edits selected category");
        putValue(MNEMONIC_KEY, KeyEvent.VK_E);
        putValue(ACCELERATOR_KEY, KeyStroke.getKeyStroke("ctrl E"));
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        var selectedRows = categoryTable.getSelectedRows();
        if (selectedRows.length != 1) {
            throw new IllegalStateException("Invalid selected rows count (must be 1): " + selectedRows.length);
        }
        if (categoryTable.isEditing()) {
            categoryTable.getCellEditor().cancelCellEditing();
        }

        var categoryTableModel = (CategoryTableModel) categoryTable.getModel();
        var selectedRow = selectedRows[0];
        var modelRow = categoryTable.convertRowIndexToModel(selectedRow);
        var category = categoryTableModel.getEntityAtIndex(modelRow);
        var oldCategory = category.createSnapshot();

        var categoryDialog = new CategoryDialog(category, "Edit category");
        Optional<Category> editedCategory;

        while (true) {
            editedCategory = categoryDialog.show(parentComponent);
            if (editedCategory.isEmpty()) {
                category.setAttributes(oldCategory);
                return;
            }

            category = editedCategory.get();
            ValidationInfo validationInfo = category.validate();
            if (validationInfo.isInvalid()) {
                JOptionPane.showMessageDialog(parentComponent,
                        validationInfo.getMessage(),
                        "Category", JOptionPane.ERROR_MESSAGE);
            } else {
                try {
                    Logger.debug("Editing category: " + category.detailedPrint());
                    editedCategory.ifPresent(BusinessLogicImpl.getInstance()::editCategory);
                    return;
                } catch (DataStorageException ex) {
                    Logger.error(ex);
                    JOptionPane.showMessageDialog(parentComponent,
                            "Unable to edit this category. Check if the name is valid and not used.",
                            "Category", JOptionPane.ERROR_MESSAGE);
                    category.setAttributes(oldCategory);
                }
            }
        }
    }
}
