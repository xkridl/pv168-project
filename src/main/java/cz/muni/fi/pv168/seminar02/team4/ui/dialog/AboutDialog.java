package cz.muni.fi.pv168.seminar02.team4.ui.dialog;

import javax.swing.*;
import java.awt.*;
import java.time.LocalDate;

public class AboutDialog extends Dialog {
    public AboutDialog(String constraints, String title, Component parentComponent) {
        super(constraints, title, parentComponent);
    }

    @Override
    public void addDialogItems() {
        String content = """
        <html>
            <hr>
            <p>This app provides an easy and efficient way to keep track of your financial balance</p>
            <br>
            <p>Authors:</p>
            <p>Jakub Judiny</p>
            <p>Adam Kridl</p>
            <p>Sophia Zapotocna</p>
            <p>Martin Suchanek</p>
            <br>
            <hr>
        </html>
        """;

        panel.add(new JLabel("Built in " + LocalDate.now().getYear()));
        var mainContent = new JLabel();
        mainContent.setText(content);
        panel.add(mainContent);
        panel.add(new JLabel("\u00a9" + LocalDate.now().getYear() + " Masaryk University FI"));
    }
}
