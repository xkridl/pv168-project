package cz.muni.fi.pv168.seminar02.team4.ui.model;

import javax.swing.*;
import javax.swing.border.CompoundBorder;
import javax.swing.border.MatteBorder;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellRenderer;
import java.awt.*;

public class TransactionTableRenderer extends DefaultTableCellRenderer implements TableCellRenderer {
    private final TransactionTableModel transactionTableModel;
    private final int columnCount;

    public TransactionTableRenderer(TransactionTableModel transactionTableModel, int columnCount) {
        this.transactionTableModel = transactionTableModel;
        this.columnCount = columnCount;
    }

    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus,
                                                   int row, int column) {
        var sortedRow = table.convertRowIndexToModel(row);
        JComponent c = (JComponent) super.getTableCellRendererComponent(table, value, isSelected, hasFocus, sortedRow, column);
        var padding = new MatteBorder(0, 5, 0, 0, new Color(0, 0, 0, 0));
        if (column % columnCount == 0) {
            Color color = transactionTableModel.getEntityAtIndex(sortedRow).getCategory() == null
                    ? Color.WHITE : transactionTableModel.getEntityAtIndex(sortedRow).getCategory().getColor();
            var colorBorder = new MatteBorder(0, 20, 0, 0, color);
            c.setBorder(new CompoundBorder(colorBorder, padding));
        } else {
            c.setBorder(padding);
        }
        return c;
    }
}
