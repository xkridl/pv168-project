package cz.muni.fi.pv168.seminar02.team4.data.manipulation;

import cz.muni.fi.pv168.seminar02.team4.model.Transaction;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.swing.*;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.awt.*;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Collection;

class XmlTransactionExporter implements TransactionExporter {
    private Component parentComponent;
    public XmlTransactionExporter(Component parentComponent) {
        this.parentComponent = parentComponent;
    }
    @Override
    public void exportTransactions(Collection<Transaction> transactions, String filePath) {
        try (FileOutputStream output = new FileOutputStream(filePath)) {
            DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
            Document doc = docBuilder.newDocument();
            Element rootElement = doc.createElement("transactions");
            doc.appendChild(rootElement);
            for (var transaction : transactions) {
                rootElement.appendChild(transaction.toXML(doc));
            }

            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            DOMSource source = new DOMSource(doc);
            StreamResult result = new StreamResult(output);
            transformer.transform(source, result);

        } catch (IOException exception) {
            JOptionPane.showMessageDialog(parentComponent,
                    "The application was unable to write to that file.",
                    "Export", JOptionPane.ERROR_MESSAGE);
        } catch (TransformerConfigurationException | ParserConfigurationException exception) {
            JOptionPane.showMessageDialog(parentComponent,
                    "Configuration error.",
                    "Export", JOptionPane.ERROR_MESSAGE);
        } catch (TransformerException exception) {
            JOptionPane.showMessageDialog(parentComponent,
                    "Transformation error.",
                    "Export", JOptionPane.ERROR_MESSAGE);
        }
    }
}
