package cz.muni.fi.pv168.seminar02.team4.ui.tabs;

import cz.muni.fi.pv168.seminar02.team4.data.storage.DataStorageException;
import cz.muni.fi.pv168.seminar02.team4.logic.BusinessLogicImpl;
import cz.muni.fi.pv168.seminar02.team4.logic.balance.BalanceCalculator;
import cz.muni.fi.pv168.seminar02.team4.logic.balance.BalanceWrapper;
import cz.muni.fi.pv168.seminar02.team4.model.Transaction;
import cz.muni.fi.pv168.seminar02.team4.ui.dialog.TransactionDialog;
import org.tinylog.Logger;

import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.border.EmptyBorder;
import java.awt.BorderLayout;
import java.awt.Component;
import java.util.Optional;

public class TransactionsTab {
    private final JComponent panel;
    private final Component parentComponent;

    public TransactionsTab(JTable table, Component parentComponent) {
        this.parentComponent = parentComponent;

        panel = new JPanel(false);
        panel.setLayout(new BorderLayout());
        panel.setBorder(new EmptyBorder(10, 20, 0, 20));

        JPanel topPanel = new JPanel();
        topPanel.setLayout(new BorderLayout());
        String labelText = "<html><span style='font-size:16px'>Transactions</span></html>";
        topPanel.add(new JLabel(labelText), BorderLayout.NORTH);
        FilterGui filterGui = new FilterGui(parentComponent);
        topPanel.add(filterGui.getPanel(), BorderLayout.CENTER);
        topPanel.setBorder(new EmptyBorder(0, 0, 20, 0));
        panel.add(topPanel, BorderLayout.NORTH);

        panel.add(new JScrollPane(table), BorderLayout.CENTER);

        JLabel filteredBalanceLabel = new JLabel();
        var filteredBalanceWrapper = new BalanceWrapper(filteredBalanceLabel, "Filtered balance");
        BalanceCalculator balanceComputer = new BalanceCalculator(filteredBalanceWrapper);
        BusinessLogicImpl.getInstance().addTransactionListener(balanceComputer);
        panel.add(filteredBalanceLabel, BorderLayout.SOUTH);
    }

    public JComponent getPanel() {
        return panel;
    }
}
