package cz.muni.fi.pv168.seminar02.team4.ui.dialog;

import cz.muni.fi.pv168.seminar02.team4.logic.BusinessLogicImpl;
import cz.muni.fi.pv168.seminar02.team4.model.Currency;

import javax.swing.*;
import java.awt.*;

public class AddCurrencyDialog extends EntityDialog<Currency> {
    private final JTextField nameField;
    private final JTextField abbreviationField;
    private final JTextField conversionRateField;

    public AddCurrencyDialog(String title) {
        super("gap 30px 10px", title );
        this.nameField = new JTextField();
        this.abbreviationField = new JTextField();
        this.conversionRateField = new JTextField("");
        conversionRateField.setBackground(BusinessLogicImpl.getInstance().getTextFieldMainColor());
        addDialogItems();
        panel.setPreferredSize(new Dimension(350, panel.getPreferredSize().height));
    }

    @Override
    public void addDialogItems() {
        add("Name", nameField, true, "w 60%, wrap, align right");
        add("Abbreviation", abbreviationField, true, "w 60%, wrap, align right");
        add("Conversion Rate", conversionRateField, true, "w 60%, wrap, align right");
        conversionRateField.addKeyListener(floatFieldListener(conversionRateField));
    }

    @Override
    Currency getEntity() {
        double conversionRate = Double.parseDouble(conversionRateField.getText()
                .replace(",", "."));
        return new Currency(nameField.getText(), abbreviationField.getText(), conversionRate);
    }
}
