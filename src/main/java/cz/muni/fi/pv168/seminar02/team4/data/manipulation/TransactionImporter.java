package cz.muni.fi.pv168.seminar02.team4.data.manipulation;

import cz.muni.fi.pv168.seminar02.team4.model.Transaction;

import java.util.Collection;

/**
 * Generic mechanism, allowing to import a {@link Collection} of {@link Transaction}
 * objects from a file.
 *
 * <p>Both the <i>format</i> and <i>encoding</i> of the file are to be defined and
 * documented by implementations of this interface.
 */
interface TransactionImporter {
    /**
     * Imports transactions from a file to an ordered {@link Collection}.
     *
     * @param filePath absolute path of the file to import
     *
     * @return imported transactions (in the same order as in the file)
     *
     * @throws DataManipulationException if the file to import does not exist,
     *         cannot be read or its format/encoding is invalid
     */
    Collection<Transaction> importTransactions(String filePath);
}
