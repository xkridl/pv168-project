package cz.muni.fi.pv168.seminar02.team4.data.storage.repository;

import cz.muni.fi.pv168.seminar02.team4.data.storage.dao.DataAccessObject;
import cz.muni.fi.pv168.seminar02.team4.data.storage.entity.CategoryEntity;
import cz.muni.fi.pv168.seminar02.team4.data.storage.mapper.EntityMapper;
import cz.muni.fi.pv168.seminar02.team4.logic.Listener;
import cz.muni.fi.pv168.seminar02.team4.logic.Publisher;
import cz.muni.fi.pv168.seminar02.team4.model.Category;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Repository of {@link Category} class.
 */
public class CategoryRepository implements Repository<Category>, Publisher<Category> {
    private final DataAccessObject<CategoryEntity> categoryDao;
    private final EntityMapper<CategoryEntity, Category> categoryMapper;
    private List<Category> categories;
    private final List<Listener<Category>> listeners;

    public CategoryRepository(
            DataAccessObject<CategoryEntity> categoryDao,
            EntityMapper<CategoryEntity, Category> categoryMapper
    ) {
        this.categoryDao = categoryDao;
        this.categoryMapper = categoryMapper;
        listeners = new ArrayList<>();
        refresh();
    }

    @Override
    public int getSize() {
        return categories.size();
    }

    @Override
    public Optional<Category> findById(long id) {
        return findAll().stream()
                .filter(category -> category.getId() == id)
                .findFirst();
    }

    @Override
    public Optional<Category> findByIndex(int index) {
        if (0 <= index && index < getSize()) {
            return Optional.of(categories.get(index));
        }
        return Optional.empty();
    }

    @Override
    public List<Category> findAll() {
        return Collections.unmodifiableList(categories);
    }

    @Override
    public void refresh() {
        categories = fetchAll();
    }

    @Override
    public void create(Category category) {
        Stream.of(category)
                .map(categoryMapper::mapToEntity)
                .map(categoryDao::create)
                .map(categoryMapper::mapToModel)
                .forEach(categories::add);
        notifyListeners(findAll());
    }

    @Override
    public void update(Category category) {
        int index = categories.indexOf(category);
        Stream.of(category)
                .map(categoryMapper::mapToEntity)
                .map(categoryDao::update)
                .map(categoryMapper::mapToModel)
                .forEach(c -> categories.set(index, c));
        notifyListeners(findAll());
    }

    @Override
    public void deleteByIndex(int index) {
        var category = categories.get(index);
        categoryDao.deleteById(category.getId());
        categories.remove(index);
        notifyListeners(findAll());
    }

    @Override
    public void delete(Category category) {
        deleteByIndex(categories.indexOf(category));
    }

    public Category findCategory(String name) {
        for (var category : findAll()) {
            if (category.getName().equals(name)) {
                return category;
            }
        }
        return null;
    }

    private List<Category> fetchAll() {
        return categoryDao.findAll()
                .stream()
                .map(categoryMapper::mapToModel)
                .collect(Collectors.toList());
    }

    @Override
    public void notifyListeners(Collection<Category> data) {
        for (var listener : listeners) {
            listener.notify(data);
        }
    }

    @Override
    public void addListener(Listener<Category> listener) {
        listeners.add(listener);
        listener.notify(findAll());
    }

    @Override
    public void removeListener(Listener<Category> listener) {
        listeners.remove(listener);
    }
}
