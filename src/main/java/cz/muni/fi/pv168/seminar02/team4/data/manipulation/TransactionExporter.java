package cz.muni.fi.pv168.seminar02.team4.data.manipulation;

import cz.muni.fi.pv168.seminar02.team4.model.Transaction;

import java.util.Collection;

/**
 * Generic mechanism, allowing to export a {@link Collection} of {@link Transaction}
 * objects to a file.
 *
 * <p>Both the <i>format</i> and <i>encoding</i> of the file are to be defined and
 * documented by implementations of this interface.
 *
 * @author Jiri Koudelka
 */
interface TransactionExporter {
    /**
     * Exports transaction from a {@link Collection} to a file.
     *
     * @param transactions to be exported (in the collection iteration order)
     * @param filePath absolute path of the export file (to be created or overwritten)
     *
     * @throws DataManipulationException if the export file cannot be opened or written
     */
    void exportTransactions(Collection<Transaction> transactions, String filePath);
}
