package cz.muni.fi.pv168.seminar02.team4.data.storage.repository;

import cz.muni.fi.pv168.seminar02.team4.data.storage.dao.DataAccessObject;
import cz.muni.fi.pv168.seminar02.team4.data.storage.entity.CurrencyEntity;
import cz.muni.fi.pv168.seminar02.team4.data.storage.mapper.EntityMapper;
import cz.muni.fi.pv168.seminar02.team4.logic.Listener;
import cz.muni.fi.pv168.seminar02.team4.logic.Publisher;
import cz.muni.fi.pv168.seminar02.team4.model.Currency;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class CurrencyRepository implements Repository<Currency>, Publisher<Currency> {
    private final DataAccessObject<CurrencyEntity> currencyDao;
    private final EntityMapper<CurrencyEntity, Currency> currencyMapper;
    private List<Currency> currencies;
    private final List<Listener<Currency>> listeners;

    public CurrencyRepository(
            DataAccessObject<CurrencyEntity> currencyDao,
            EntityMapper<CurrencyEntity, Currency> currencyMapper
    ) {
        this.currencyDao = currencyDao;
        this.currencyMapper = currencyMapper;
        listeners = new ArrayList<>();
        refresh();
    }

    @Override
    public int getSize() {
        return currencies.size();
    }

    @Override
    public Optional<Currency> findById(long id) {
        return findAll().stream()
                .filter(currency -> currency.getId() == id)
                .findFirst();
    }

    @Override
    public Optional<Currency> findByIndex(int index) {
        if (0 <= index && index < getSize()) {
            return Optional.of(currencies.get(index));
        }
        return Optional.empty();
    }

    @Override
    public List<Currency> findAll() {
        return Collections.unmodifiableList(currencies);
    }

    @Override
    public void refresh() {
        currencies = new ArrayList<>(fetchAll());
    }

    @Override
    public void create(Currency currency) {
        Stream.of(currency)
                .map(currencyMapper::mapToEntity)
                .map(currencyDao::create)
                .map(currencyMapper::mapToModel)
                .forEach(currencies::add);
        notifyListeners(findAll());
    }

    @Override
    public void update(Currency currency) {
        int index = currencies.indexOf(currency);
        Stream.of(currency)
                .map(currencyMapper::mapToEntity)
                .map(currencyDao::update)
                .map(currencyMapper::mapToModel)
                .forEach(c -> currencies.set(index, c));
        notifyListeners(findAll());
    }

    @Override
    public void deleteByIndex(int index) {
        var currency = currencies.get(index);
        currencyDao.deleteById(currency.getId());
        currencies.remove(index);
        notifyListeners(findAll());
    }

    @Override
    public void delete(Currency currency) {
        deleteByIndex(currencies.indexOf(currency));
    }

    public void removeCurrency(String abbreviation) {
        currencies.stream()
                .filter(currency -> currency.getAbbreviation().equals(abbreviation))
                .forEach(this::delete);
    }

    public Currency getCurrency(String abbreviation) {
        for (Currency currency : currencies) {
            if (currency.getAbbreviation().equals(abbreviation)) {
                return currency;
            }
        }
        return null;
    }

    private List<Currency> fetchAll() {
        return currencyDao.findAll()
                .stream()
                .map(currencyMapper::mapToModel)
                .collect(Collectors.toList());
    }

    @Override
    public void notifyListeners(Collection<Currency> currencies) {
        for (var listener : listeners) {
            listener.notify(currencies);
        }
    }

    @Override
    public void addListener(Listener<Currency> listener) {
        listeners.add(listener);
        listener.notify(findAll());
    }

    @Override
    public void removeListener(Listener<Currency> listener) {
        listeners.remove(listener);
    }
}
