package cz.muni.fi.pv168.seminar02.team4.ui.model;

import cz.muni.fi.pv168.seminar02.team4.logic.Listener;

import javax.swing.ListModel;
import java.util.Collection;

public interface ListeningListModel<E> extends ListModel<E>, Listener<E> {
    Collection<E> getEntities();
    void setEntities(Collection<E> entities);
}
