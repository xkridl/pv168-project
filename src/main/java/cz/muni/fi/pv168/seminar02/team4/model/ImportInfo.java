package cz.muni.fi.pv168.seminar02.team4.model;

import cz.muni.fi.pv168.seminar02.team4.model.enums.FileFormat;

import java.io.File;

/**
 * Class encapsulating all the necessary information where to export
 * the data, i.e. file, and how to export this data, e.g. in XML format.
 *
 * @author Adam Kridl
 */
public class ImportInfo {
    private final File importFrom;
    private final FileFormat importFileFormat;

    public ImportInfo(File importFrom, FileFormat importFileFormat) {
        this.importFrom = importFrom;
        this.importFileFormat = importFileFormat;
    }

    public File getImportFrom() {
        return importFrom;
    }

    public FileFormat getImportFileFormat() {
        return importFileFormat;
    }
}
