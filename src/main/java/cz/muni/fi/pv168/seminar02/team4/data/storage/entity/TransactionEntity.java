package cz.muni.fi.pv168.seminar02.team4.data.storage.entity;

import cz.muni.fi.pv168.seminar02.team4.model.Transaction;
import cz.muni.fi.pv168.seminar02.team4.model.enums.Period;
import cz.muni.fi.pv168.seminar02.team4.model.enums.TransactionType;

import java.time.LocalDate;

/**
 * {@link Transaction} representation in the persistent layer.
 */
public record TransactionEntity(
        Long id,
        long categoryId,
        long currencyId,
        String name,
        String description,
        Double amount,
        LocalDate date,
        TransactionType transactionType,
        Period period
) {
    public TransactionEntity(
            long categoryId,
            long currencyId,
            String name,
            String description,
            Double amount,
            LocalDate date,
            TransactionType transactionType,
            Period period
    ) {
        this(null, categoryId, currencyId ,name, description, amount, date, transactionType, period);
    }
}
