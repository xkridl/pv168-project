package cz.muni.fi.pv168.seminar02.team4.logic;

import java.util.Collection;

/**
 * Generic listener interface for using observer pattern between
 * EntityManipulator<E> and EntityTableModel<E>.
 *
 * @author Adam Kridl
 */
public interface Listener<E> {
    void notify(Collection<E> data);
}
