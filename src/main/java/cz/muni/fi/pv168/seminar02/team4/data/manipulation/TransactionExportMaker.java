package cz.muni.fi.pv168.seminar02.team4.data.manipulation;

import cz.muni.fi.pv168.seminar02.team4.model.ExportInfo;
import cz.muni.fi.pv168.seminar02.team4.model.Transaction;

import javax.swing.*;
import java.awt.*;
import java.util.Collection;

/**
 * Class doing the export thanks to corresponding classes
 * implementing the {@link TransactionExporter} interface.
 *
 * @author Adam Kridl
 */
public class TransactionExportMaker {
    private TransactionExporter transactionExporter;
    private final Component parentComponent;
    public TransactionExportMaker(Component parentComponent) {
        this.parentComponent = parentComponent;
    }
    public void setTransactionExporterStrategy(TransactionExporter transactionExporter) {
        this.transactionExporter = transactionExporter;
    }
    public void doExport(ExportInfo exportInfo, Collection<Transaction> transactions) {
        switch (exportInfo.getExportForm()) {
            case CSV -> setTransactionExporterStrategy(new CsvTransactionExporter(parentComponent));
            case JSON -> setTransactionExporterStrategy(new JsonTransactionExporter(parentComponent));
            case XML -> setTransactionExporterStrategy(new XmlTransactionExporter(parentComponent));
        }
        transactionExporter.exportTransactions(transactions, exportInfo.getExportTo().getAbsolutePath());
    }

    public static JTextField getTextFieldFromFileChooser(JFileChooser fileChooser) {
        return (JTextField) (
                    (Container) (
                        (Container) (
                                (Container) fileChooser
                                        .getComponent(1))
                                .getComponent(2))
                        .getComponent(0))
                .getComponent(1);
    }
}
