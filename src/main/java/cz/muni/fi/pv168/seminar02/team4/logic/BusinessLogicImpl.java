package cz.muni.fi.pv168.seminar02.team4.logic;

import cz.muni.fi.pv168.seminar02.team4.data.manipulation.TransactionExportMaker;
import cz.muni.fi.pv168.seminar02.team4.data.manipulation.TransactionImportMaker;
import cz.muni.fi.pv168.seminar02.team4.data.storage.repository.CategoryRepository;
import cz.muni.fi.pv168.seminar02.team4.data.storage.repository.CurrencyRepository;
import cz.muni.fi.pv168.seminar02.team4.data.storage.repository.TransactionRepository;
import cz.muni.fi.pv168.seminar02.team4.model.Category;
import cz.muni.fi.pv168.seminar02.team4.model.Currency;
import cz.muni.fi.pv168.seminar02.team4.model.ExportInfo;
import cz.muni.fi.pv168.seminar02.team4.model.ImportInfo;
import cz.muni.fi.pv168.seminar02.team4.model.Transaction;
import cz.muni.fi.pv168.seminar02.team4.model.enums.Theme;

import javax.swing.JOptionPane;
import javax.swing.SwingWorker;
import java.awt.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.concurrent.ExecutionException;

/**
 * Class doing all the business logic.
 */
public class BusinessLogicImpl implements BusinessLogic {
    private static BusinessLogicImpl instance;
    private CategoryRepository categoryRepository;
    private TransactionRepository transactionRepository;
    private CurrencyRepository currencyRepository;
    private final TransactionExportMaker transactionExportMaker;
    private final TransactionImportMaker transactionImportMaker;
    private Theme currentTheme = Theme.FLAT_LAF_LIGHT;
    private Color textFieldMainColor = Color.white;
    private static Component rootComponent;
    private BusinessLogicImpl() {
        // Intentionally made private to prevent instantiation.
        transactionExportMaker = new TransactionExportMaker(rootComponent);
        transactionImportMaker = new TransactionImportMaker(rootComponent);
    }

    public static BusinessLogicImpl getInstance() {
        if (instance == null) {
            instance = new BusinessLogicImpl();
        }
        return instance;
    }

    public static void setRootComponent(Component rootComponent) {
        BusinessLogicImpl.rootComponent = rootComponent;
    }

    public CategoryRepository getCategoryRepository() {
        return categoryRepository;
    }

    public CurrencyRepository getCurrencyRepository() {
        return currencyRepository;
    }

    public TransactionRepository getTransactionRepository() {
        return transactionRepository;
    }

    @Override
    public void addCategory(Category category) {
        categoryRepository.create(category);
    }

    @Override
    public void editCategory(Category category) {
        categoryRepository.update(category);
    }

    @Override
    public void deleteCategories(Collection<Category> categories) {
        categories.forEach(this::deleteCategory);
    }

    private void deleteCategory(Category category) {
        categoryRepository.delete(category);
    }

    @Override
    public void addTransaction(Transaction transaction) {
        transactionRepository.create(transaction);
    }

    @Override
    public void editTransaction(Transaction transaction) {
        transactionRepository.update(transaction);
    }

    @Override
    public void deleteTransactions(Collection<Transaction> transactions) {
        transactions
                .forEach(this::deleteTransaction);
    }

    private void deleteTransaction(Transaction transaction) {
        transactionRepository.delete(transaction);
    }

    @Override
    public void doExport(ExportInfo exportInfo) {
        var asyncExporter = new SwingWorker<Void, Void>() {
            @Override
            protected Void doInBackground() {
                transactionExportMaker.doExport(exportInfo, BusinessLogicImpl.getInstance().getTransactions());
                return null;
            }

            @Override
            protected void done() {
                JOptionPane.showMessageDialog(rootComponent, "Transactions export was successful.", "Export",JOptionPane.INFORMATION_MESSAGE);
            }
        };
        asyncExporter.execute();
    }

    @Override
    public void doImport(ImportInfo importInfo) {
        var asyncImporter = new SwingWorker<Collection<Transaction>, Void>() {
            @Override
            protected Collection<Transaction> doInBackground() {
                return transactionImportMaker.doImport(importInfo);
            }

            @Override
            protected void done() {
                Collection<Transaction> importedTransactions;
                try {
                    importedTransactions = get();

                } catch (InterruptedException e) {
                    JOptionPane.showMessageDialog(rootComponent, "Unable to import such transactions");
                    return;
                } catch (ExecutionException e) {
                    return;
                }

                if (importedTransactions.size() > 0) {
                    Object[] options = {"Merge", "Overwrite", "Abort"};
                    if (getTransactions().size() == 0) {
                        addTransactions(importedTransactions);
                    } else {
                        var option = JOptionPane.showOptionDialog(rootComponent,
                                "Transactions imported successfully. Do you want to merge them with " +
                                        "the old ones or overwrite them?",
                                "Import transactions", JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE,
                                null, options, options[0]);
                        if (option == JOptionPane.YES_OPTION) {
                            addTransactions(importedTransactions);
                        } else if (option == JOptionPane.NO_OPTION) {
                            deleteTransactions(new ArrayList<>(transactionRepository.findAll()));
                            addTransactions(importedTransactions);
                        }
                    }
                }
            }
        };
        asyncImporter.execute();
    }

    private void addTransactions(Collection<Transaction> transactions) {
        var asyncTransactionsAdder = new SwingWorker<Void, Void>() {
            @Override
            protected Void doInBackground() {
                transactions.forEach(BusinessLogicImpl.getInstance()::addTransaction);
                return null;
            }
        };
        asyncTransactionsAdder.execute();
    }

    public Collection<Transaction> getTransactions() {
        return transactionRepository.getMatchingEntities();
    }

    public Collection<Currency> getCurrencies() {
        return currencyRepository.findAll();
    }

    @Override
    public void updateTransactionFilter(FilterBL filterBL) {
        transactionRepository.setFilterBL(filterBL);
    }

    public void recomputeBalance() {
        transactionRepository.recomputeBalance();
    }

    public void addCategoryListener(Listener<Category> categoryListener) {
        categoryRepository.addListener(categoryListener);
    }

    public void addTransactionListener(Listener<Transaction> transactionListener) {
        transactionRepository.addListenerOfFiltered(transactionListener);
    }

    public void addTransactionListenerOfAll(Listener<Transaction> allTransactionsListener) {
        transactionRepository.addListenerOfAll(allTransactionsListener);
    }

    public void addCurrencyListener(Listener<Currency> currencyListener) {
        currencyRepository.addListener(currencyListener);
    }

    private void setCategoryRepository(CategoryRepository categoryRepository) {
        // Intentionally made private, use builder instead
        this.categoryRepository = categoryRepository;
    }

    private void setTransactionRepository(TransactionRepository transactionRepository) {
        // Intentionally made private, use builder instead
        this.transactionRepository = transactionRepository;
    }

    private void setCurrencyRepository(CurrencyRepository currencyRepository) {
        // Intentionally made private, use builder instead
        this.currencyRepository = currencyRepository;
    }

    public static class BusinessLogicBuilder {
        private final BusinessLogicImpl businessLogicImpl;

        public BusinessLogicBuilder() {
            businessLogicImpl = getInstance();
        }

        public BusinessLogicBuilder withCategoryRepository(CategoryRepository categoryRepository) {
            businessLogicImpl.setCategoryRepository(categoryRepository);
            return this;
        }

        public BusinessLogicBuilder withTransactionRepository(TransactionRepository transactionRepository) {
            businessLogicImpl.setTransactionRepository(transactionRepository);
            return this;
        }

        public BusinessLogicBuilder withCurrencyRepository(CurrencyRepository currencyRepository) {
            businessLogicImpl.setCurrencyRepository(currencyRepository);
            return this;
        }

        public BusinessLogicImpl build() {
            return businessLogicImpl;
        }
    }

    public Theme getCurrentTheme() {return currentTheme;}

    public Color getTextFieldMainColor() {
        return textFieldMainColor;
    }
    public void setTextFieldMainColor(Color color) {
        this.textFieldMainColor = color;
    }

    public void setCurrentTheme(Theme theme) {currentTheme = theme;}
}
