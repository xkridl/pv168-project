package cz.muni.fi.pv168.seminar02.team4.data.storage.dao;

import cz.muni.fi.pv168.seminar02.team4.data.storage.DataStorageException;
import cz.muni.fi.pv168.seminar02.team4.data.storage.db.ConnectionHandler;
import cz.muni.fi.pv168.seminar02.team4.data.storage.entity.TransactionEntity;
import cz.muni.fi.pv168.seminar02.team4.model.enums.Period;
import cz.muni.fi.pv168.seminar02.team4.model.enums.TransactionType;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.function.Supplier;

public class TransactionDao implements DataAccessObject<TransactionEntity> {
    private final Supplier<ConnectionHandler> connections;

    public TransactionDao(Supplier<ConnectionHandler> connections) {
        this.connections = connections;
    }

    @Override
    public TransactionEntity create(TransactionEntity entity) {
        var sql = """
                INSERT INTO Transaction(
                                categoryId,
                                currencyId,
                                name,
                                description,
                                amount,
                                date,
                                transactionType,
                                period)
                VALUES (?, ?, ?, ?, ?, ?, ?, ?);
                """;

        try (
                var connection = connections.get();
                var statement = connection.use().prepareStatement(sql, Statement.RETURN_GENERATED_KEYS)
        ) {
            statement.setLong(1, entity.categoryId());
            statement.setLong(2, entity.currencyId());
            statement.setString(3, entity.name());
            statement.setString(4, entity.description());
            statement.setDouble(5, entity.amount());
            statement.setDate(6, Date.valueOf(entity.date()));
            statement.setString(7, entity.transactionType().toString());
            statement.setString(8, entity.period() != null ? entity.period().toString() : "NO PERIOD");
            statement.executeUpdate();

            try (ResultSet keyResultSet = statement.getGeneratedKeys()) {
                long transactionId;

                if (keyResultSet.next()) {
                    transactionId = keyResultSet.getLong(1);
                } else {
                    throw new DataStorageException("Failed to fetch generated key for: " + entity);
                }
                if (keyResultSet.next()) {
                    throw new DataStorageException("Multiple keys returned for: " + entity);
                }
                return findById(transactionId).orElseThrow();
            }
        } catch (SQLException ex) {
            throw new DataStorageException("Failed to store: " + entity, ex);
        }
    }

    @Override
    public Collection<TransactionEntity> findAll() {
        var sql = """
                SELECT  id,
                        categoryId,
                        currencyId,
                        name,
                        description,
                        amount,
                        date,  
                        transactionType,
                        period
                FROM Transaction;
                """;

        try (
                var connection = connections.get();
                var statement = connection.use().prepareStatement(sql)) {

            List<TransactionEntity> transactions = new ArrayList<>();
            try (var resultSet = statement.executeQuery()) {
                while (resultSet.next()) {
                    var transaction = transactionFromResultSet(resultSet);
                    transactions.add(transaction);
                }
            }

            return transactions;
        } catch (SQLException ex) {
            throw new DataStorageException("Failed to load all transactions", ex);
        }
    }

    @Override
    public Optional<TransactionEntity> findById(long id) {
        var sql = """
                SELECT  id,
                        categoryId,
                        currencyId,
                        name,
                        description,
                        amount,
                        date,
                        transactionType,
                        period
                FROM Transaction
                WHERE id = ?;
                """;
        try (
                var connection = connections.get();
                var statement = connection.use().prepareStatement(sql)
        ) {
            statement.setLong(1, id);
            var resultSet = statement.executeQuery();
            if (resultSet.next()) {
                return Optional.of(transactionFromResultSet(resultSet));
            } else {
                // category not found
                return Optional.empty();
            }
        } catch (
                SQLException ex) {
            throw new DataStorageException("Failed to load transaction by id: " + id, ex);
        }
    }

    @Override
    public TransactionEntity update(TransactionEntity entity) {
        var sql = """
                UPDATE Transaction
                SET categoryId=?,currencyId=?,name=?, description=?,amount=?,date=?,transactionType=?, period=? 
                WHERE id=?;
                """;
        try (
                var connection = connections.get();
                var statement = connection.use().prepareStatement(sql)
        ) {
            statement.setLong(1, entity.categoryId());
            statement.setLong(2, entity.currencyId());
            statement.setString(3, entity.name());
            statement.setString(4, entity.description());
            statement.setDouble(5, entity.amount());
            statement.setDate(6, Date.valueOf(entity.date()));
            statement.setString(7, entity.transactionType().toString());
            statement.setString(8, entity.period() != null ? entity.period().toString() : "NO PERIOD");
            statement.setLong(9, entity.id());
            var recordsChanged = statement.executeUpdate();

            if (recordsChanged == 0) {
                throw new DataStorageException("Failed to fetch generated key for: " + entity);
            } else if (recordsChanged > 1) {
                throw new DataStorageException("Multiple keys returned for: " + entity);
            }
            return findById(entity.id()).orElseThrow();
        } catch (SQLException ex) {
            throw new DataStorageException("Failed to update entity: " + entity, ex);
        }
    }

    @Override
    public void deleteById(long entityId) {
        var sql = """
                DELETE FROM Transaction
                WHERE id = ?;
                """;

        try (
                var connection = connections.get();
                var statement = connection.use().prepareStatement(sql)
        ) {
            statement.setLong(1, entityId);
            int rowsUpdated = statement.executeUpdate();
            if (rowsUpdated == 0) {
                throw new DataStorageException("Transaction not found %d".formatted(entityId));
            }
            if (rowsUpdated > 1) {
                throw new DataStorageException(
                        "More then 1 transaction (rows=%d) has been deleted: %d".formatted(rowsUpdated, entityId));
            }
        } catch (SQLException ex) {
            throw new DataStorageException("Failed to delete transaction %d".formatted(entityId), ex);
        }
    }

    private static TransactionEntity transactionFromResultSet(ResultSet resultSet) throws SQLException {
        return new TransactionEntity(
                resultSet.getLong("id"),
                resultSet.getLong("categoryId"),
                resultSet.getLong("currencyId"),
                resultSet.getString("name"),
                resultSet.getString("description"),
                resultSet.getDouble("amount"),
                resultSet.getDate("date").toLocalDate(),
                resultSet.getString("transactionType").equals("Incoming") ? TransactionType.INCOMING : TransactionType.OUTGOING,
                Period.parse(resultSet.getString("period"))
        );
    }
}
