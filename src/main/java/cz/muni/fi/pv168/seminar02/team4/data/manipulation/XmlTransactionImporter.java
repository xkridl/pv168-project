package cz.muni.fi.pv168.seminar02.team4.data.manipulation;

import cz.muni.fi.pv168.seminar02.team4.model.Amount;
import cz.muni.fi.pv168.seminar02.team4.model.Category;
import cz.muni.fi.pv168.seminar02.team4.model.Transaction;
import cz.muni.fi.pv168.seminar02.team4.model.enums.Period;
import cz.muni.fi.pv168.seminar02.team4.model.enums.TransactionType;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.swing.*;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.awt.*;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

class XmlTransactionImporter implements TransactionImporter {
    private final Component parentComponent;
    public XmlTransactionImporter(Component parentComponent) {
        this.parentComponent = parentComponent;
    }
    private static Transaction parseXMLElement(Element transaction) {
        return new Transaction(
                transaction.getElementsByTagName("name").item(0).getTextContent(),
                transaction.getElementsByTagName("description").item(0).getTextContent(),
                Amount.parse(transaction.getElementsByTagName("amount").item(0).getTextContent()),
                LocalDate.parse(transaction.getElementsByTagName("date").item(0).getTextContent()),
                Category.parse(transaction.getElementsByTagName("category").item(0).getTextContent()),
                transaction.getElementsByTagName("type").item(0).getTextContent().equals("Incoming") ?
                        TransactionType.INCOMING : TransactionType.OUTGOING,
                transaction.getElementsByTagName("period").item(0).getTextContent().equals("") ? null :
                        Period.parse(transaction.getElementsByTagName("period").item(0).getTextContent()));
    }

    @Override
    public Collection<Transaction> importTransactions(String filePath) {
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        try {
            DocumentBuilder db = dbf.newDocumentBuilder();
            Document doc = db.parse(new File(filePath));
            List<Transaction> transactions = new ArrayList<>();
            NodeList transactionsList = doc.getElementsByTagName("transaction");
            for (int i = 0; i < transactionsList.getLength(); i++) {
                transactions.add(parseXMLElement((Element) transactionsList.item(i)));
            }

            return transactions;
        } catch (FileNotFoundException exception) {
            JOptionPane.showMessageDialog(parentComponent,
                    "File does not exist.",
                    "Import", JOptionPane.ERROR_MESSAGE);
            throw new DataManipulationException("File does not exist", exception);
        } catch (ParserConfigurationException | SAXException exception) {
            JOptionPane.showMessageDialog(parentComponent,
                    "Configuration error.",
                    "Import", JOptionPane.ERROR_MESSAGE);
            throw new DataManipulationException("Configuration error.", exception);
        } catch (IOException exception) {
            JOptionPane.showMessageDialog(parentComponent,
                    "Unable to read file.",
                    "Import", JOptionPane.ERROR_MESSAGE);
            throw new DataManipulationException("Unable to read file", exception);
        } catch (IllegalArgumentException exception) {
            JOptionPane.showMessageDialog(parentComponent,
                    "Invalid data structure.",
                    "Import", JOptionPane.ERROR_MESSAGE);
            throw new DataManipulationException("Invalid data structure.", exception);
        }
    }
}
