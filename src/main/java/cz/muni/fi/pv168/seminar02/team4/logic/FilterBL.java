package cz.muni.fi.pv168.seminar02.team4.logic;

import cz.muni.fi.pv168.seminar02.team4.model.Transaction;

import java.time.LocalDate;

/**
 * Business logic for filter.
 */

public class FilterBL {
    private boolean incoming;
    private boolean outgoing;
    private LocalDate fromDate;
    private LocalDate toDate;
    private String category;
    private String currency;
    private boolean ordinary;
    private boolean repeating;
    private String period;

    public FilterBL() {}

    public void setIncoming(boolean incoming) {
        this.incoming = incoming;
    }

    public void setOutgoing(boolean outgoing) {
        this.outgoing = outgoing;
    }

    public void setFromDate(LocalDate fromDate) {
        this.fromDate = fromDate;
    }

    public void setToDate(LocalDate toDate) {
        this.toDate = toDate;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public void setOrdinary(boolean ordinary) {
        this.ordinary = ordinary;
    }

    public void setRepeating(boolean repeating) {
        this.repeating = repeating;
    }

    public void setPeriod(String period) {
        this.period = period;
    }

    public boolean include(Transaction transaction) {
        return (satisfyingIncomingType(transaction) || satisfyingOutgoingType(transaction)) &&
                (satisfyingOrdinaryType(transaction) || satisfyingRepeatingType(transaction)) &&
                satisfyingCategory(transaction) &&
                satisfyingCurrency(transaction) &&
                satisfyingPeriod(transaction) &&
                satisfyingFromDate(transaction) &&
                satisfyingToDate(transaction);
    }

    private boolean satisfyingIncomingType(Transaction transaction) {
        return incoming && transaction.isIncoming();
    }

    private boolean satisfyingOutgoingType(Transaction transaction) {
        return outgoing && transaction.isOutgoing();
    }

    private boolean satisfyingOrdinaryType(Transaction transaction) {
        return ordinary && transaction.isOrdinary();
    }

    private boolean satisfyingRepeatingType(Transaction transaction) {
        return repeating && transaction.isRepeating();
    }

    private boolean satisfyingCategory(Transaction transaction) {
        if (category.equals("Any")) {
            return true;
        }
        return transaction.getCategory().getName().equals(category);
    }

    private boolean satisfyingCurrency(Transaction transaction) {
        if (currency.equals("Any")) {
            return true;
        }
        return transaction.getAmount().getCurrency().getAbbreviation().equals(currency);
    }

    private boolean satisfyingPeriod(Transaction transaction) {
        if (repeating) {
            var transactionPeriod = transaction.getPeriod();
            if (transactionPeriod == null || period.equals("Any")) {
                return true;
            } else {
                return transactionPeriod.toString().equals(period);
            }
        } else {
            return true;
        }
    }

    private boolean satisfyingFromDate(Transaction transaction) {
        if (fromDate == null) {
            return true;
        }
        return transaction.getDate().isAfter(fromDate) || transaction.getDate().isEqual(fromDate);
    }

    private boolean satisfyingToDate(Transaction transaction) {
        if (toDate == null) {
            return true;
        }
        return transaction.getDate().isBefore(toDate) || transaction.getDate().isEqual(toDate);
    }
}
