package cz.muni.fi.pv168.seminar02.team4.ui.model;

import javax.swing.JComponent;
import javax.swing.JTable;
import javax.swing.border.CompoundBorder;
import javax.swing.border.MatteBorder;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellRenderer;
import java.awt.Color;
import java.awt.Component;

public class CategoryTableRenderer extends DefaultTableCellRenderer implements TableCellRenderer {
    private final CategoryTableModel categoryTableModel;

    public CategoryTableRenderer(CategoryTableModel categoryTableModel) {
        this.categoryTableModel = categoryTableModel;
    }

    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus,
                                                   int row, int column) {
        var sortedRow = table.convertRowIndexToModel(row);
        JComponent c = (JComponent) super.getTableCellRendererComponent(table, value, isSelected, hasFocus, sortedRow, column);
        var colorBorder = new MatteBorder(0, 20, 0, 0, categoryTableModel.getEntityAtIndex(sortedRow).getColor());
        var padding = new MatteBorder(0, 5, 0, 0, new Color(0, 0, 0, 0));
        c.setBorder(new CompoundBorder(colorBorder, padding));
        return c;
    }
}
