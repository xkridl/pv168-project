package cz.muni.fi.pv168.seminar02.team4.ui.action;

import cz.muni.fi.pv168.seminar02.team4.logic.BusinessLogicImpl;
import cz.muni.fi.pv168.seminar02.team4.model.ImportInfo;
import cz.muni.fi.pv168.seminar02.team4.model.enums.FileFormat;

import javax.swing.AbstractAction;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.event.ActionEvent;


public class ImportJSONAction extends AbstractAction {

    private final JFrame frame;

    public ImportJSONAction(JFrame frame) {
        super("Import from JSON", null);
        this.frame = frame;
        putValue(SHORT_DESCRIPTION, "Imports transactions from a JSON file");
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        var fileChooser = new JFileChooser();
        FileNameExtensionFilter filter = new FileNameExtensionFilter(
                "JSON files", "json");
        fileChooser.setFileFilter(filter);
        int dialogResult = fileChooser.showOpenDialog(frame);
        if (dialogResult == JFileChooser.APPROVE_OPTION) {
            var importInfo = new ImportInfo(fileChooser.getSelectedFile(), FileFormat.JSON);
            BusinessLogicImpl.getInstance().doImport(importInfo);
        }
    }
}