package cz.muni.fi.pv168.seminar02.team4.data.manipulation;

import cz.muni.fi.pv168.seminar02.team4.model.ImportInfo;
import cz.muni.fi.pv168.seminar02.team4.model.Transaction;

import java.awt.*;
import java.util.Collection;

/**
 * Class doing the import thanks to corresponding classes
 * implementing the {@link TransactionImporter} interface.
 *
 * @author Adam Kridl
 */
public class TransactionImportMaker {
    private TransactionImporter transactionImporter;

    private Component parentComponent;

    public TransactionImportMaker(Component parentComponent) {
        this.parentComponent = parentComponent;
    }


    public void setTransactionImporterStrategy(TransactionImporter transactionImporter) {
        this.transactionImporter = transactionImporter;
    }

    public Collection<Transaction> doImport(ImportInfo importInfo) {
        switch (importInfo.getImportFileFormat()) {
            case CSV -> setTransactionImporterStrategy(new CsvTransactionImporter(parentComponent));
            case JSON -> setTransactionImporterStrategy(new JsonTransactionImporter(parentComponent));
            case XML -> setTransactionImporterStrategy(new XmlTransactionImporter(parentComponent));
        }
        return transactionImporter.importTransactions(importInfo.getImportFrom().getAbsolutePath());
    }
}
