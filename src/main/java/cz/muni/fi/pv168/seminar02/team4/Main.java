package cz.muni.fi.pv168.seminar02.team4;

import com.formdev.flatlaf.FlatLightLaf;
import cz.muni.fi.pv168.seminar02.team4.ui.MainWindow;
import cz.muni.fi.pv168.seminar02.team4.wiring.DependencyProvider;
import cz.muni.fi.pv168.seminar02.team4.wiring.ProductionDependencyProvider;

import java.awt.EventQueue;

/**
 * @author Jakub Judiny, Sophia Zapotocna, Martin Suchanek, Adam Kridl
 */
public class Main {
    public static void main(String[] args) {
        FlatLightLaf.setup();
        final DependencyProvider dependencyProvider = new ProductionDependencyProvider();
        EventQueue.invokeLater(() -> new MainWindow().show());
    }
}