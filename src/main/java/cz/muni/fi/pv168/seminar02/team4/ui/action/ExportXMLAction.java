package cz.muni.fi.pv168.seminar02.team4.ui.action;

import cz.muni.fi.pv168.seminar02.team4.data.manipulation.TransactionExportMaker;
import cz.muni.fi.pv168.seminar02.team4.logic.BusinessLogicImpl;
import cz.muni.fi.pv168.seminar02.team4.model.ExportInfo;
import cz.muni.fi.pv168.seminar02.team4.model.enums.FileFormat;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.io.File;


public class ExportXMLAction extends AbstractAction {

    private final JFrame frame;

    public ExportXMLAction(JFrame frame) {
        super("Export As XML", null);
        this.frame = frame;
        putValue(SHORT_DESCRIPTION, "Exports transactions to a XML file");
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        var fileChooser = new JFileChooser();
        var textField = TransactionExportMaker.getTextFieldFromFileChooser(fileChooser);
        fileChooser.setSelectedFile(new File(".xml"));
        SwingUtilities.invokeLater(() -> textField.setCaretPosition(0));
        int dialogResult = fileChooser.showSaveDialog(frame);
        if (dialogResult == JFileChooser.APPROVE_OPTION) {
            var exportInfo = new ExportInfo(fileChooser.getSelectedFile(), FileFormat.XML);
            BusinessLogicImpl.getInstance().doExport(exportInfo);
        }
    }
}