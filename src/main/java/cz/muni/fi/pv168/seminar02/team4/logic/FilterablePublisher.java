package cz.muni.fi.pv168.seminar02.team4.logic;

import java.util.Collection;

/**
 * Special kind of {@link Publisher<E>}, which is able to distinguish
 * listeners who wish to listen only filtered data and listeners who
 * wish to listen all the data.
 *
 * @author Adam Kridl
 */
public interface FilterablePublisher<E> {
    void notifyListenersOfFiltered(Collection<E> data);
    void notifyListenersOfAll(Collection<E> data);
    void addListenerOfFiltered(Listener<E> listener);
    void addListenerOfAll(Listener<E> listener);
    void removeListenerOfFiltered(Listener<E> listener);
    void removeListenerOfAll(Listener<E> listener);
}
