package cz.muni.fi.pv168.seminar02.team4.ui;

import cz.muni.fi.pv168.seminar02.team4.logic.BusinessLogicImpl;
import cz.muni.fi.pv168.seminar02.team4.logic.balance.BalanceCalculator;
import cz.muni.fi.pv168.seminar02.team4.logic.balance.BalanceWrapper;
import cz.muni.fi.pv168.seminar02.team4.ui.action.*;
import cz.muni.fi.pv168.seminar02.team4.ui.dialog.CurrenciesDialog;
import cz.muni.fi.pv168.seminar02.team4.ui.model.CategoryTableModel;
import cz.muni.fi.pv168.seminar02.team4.ui.model.CategoryTableRenderer;
import cz.muni.fi.pv168.seminar02.team4.ui.model.TransactionTableModel;
import cz.muni.fi.pv168.seminar02.team4.ui.model.TransactionTableRenderer;
import cz.muni.fi.pv168.seminar02.team4.ui.tabs.CategoriesTab;
import cz.muni.fi.pv168.seminar02.team4.ui.tabs.TransactionsTab;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.util.Comparator;
import java.util.Objects;

public class MainWindow {
    private final JFrame frame;
    private final JTable categoriesTable;
    private final JTable transactionsTable;
    private final CategoriesTab categoriesTab;
    private final TransactionsTab transactionsTab;
    private final JTabbedPane tabbedPane;
    private final Action aboutAction;
    private final Action settingsAction;
    private final Action addTransactionAction;
    private final Action editTransactionAction;
    private final Action deleteTransactionAction;
    private final Action addCategoryAction;
    private final Action editCategoryAction;
    private final Action deleteCategoryAction;
    private final Action exportCSVAction;
    private final Action exportXMLAction;
    private final Action exportJSONAction;
    private final Action importCSVAction;
    private final Action importXMLAction;
    private final Action importJSONAction;

    public MainWindow() {
        frame = createFrame();
        ImageIcon logo = new ImageIcon(Objects.requireNonNull(MainWindow.class.getResource("app_icon.png")));
        frame.setIconImage(logo.getImage());

        categoriesTable = createCategoriesTable();
        transactionsTable = createTransactionsTable();

        addCategoryAction = new AddCategoryAction(frame);
        editCategoryAction = new EditCategoryAction(frame, categoriesTable);
        deleteCategoryAction = new DeleteCategoryAction(frame, categoriesTable);

        addTransactionAction = new AddTransactionAction(frame);
        editTransactionAction = new EditTransactionAction(frame, transactionsTable);
        deleteTransactionAction = new DeleteTransactionAction(frame, transactionsTable);

        aboutAction = new AboutAction(frame);
        settingsAction = new SettingsAction(frame);

        exportCSVAction = new ExportCSVAction(frame);
        exportJSONAction = new ExportJSONAction(frame);
        exportXMLAction = new ExportXMLAction(frame);

        importCSVAction = new ImportCSVAction(frame);
        importJSONAction = new ImportJSONAction(frame);
        importXMLAction = new ImportXMLAction(frame);

        categoriesTab = createCategoriesTab();
        transactionsTab = createTransactionsTab();

        frame.setJMenuBar(createMenuBar());
        frame.add(createTopPanel(), BorderLayout.NORTH);
        this.tabbedPane = createTabPane();
        frame.add(tabbedPane);
        changeActionsState(0);

        BusinessLogicImpl.setRootComponent(frame);
    }

    public void show() {
        frame.setVisible(true);
    }

    private JFrame createFrame() {
        var frame = new JFrame("Cash Flow Evidence");
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.setLayout(new BorderLayout());
        frame.setSize(1920, 1080);
        frame.setLocationRelativeTo(null);
        frame.setMinimumSize(new Dimension(320, 320));
        frame.setExtendedState(JFrame.MAXIMIZED_BOTH);
        return frame;
    }

    private TransactionsTab createTransactionsTab() {
        transactionsTable.setComponentPopupMenu(createTransactionsTablePopupMenu());
        return new TransactionsTab(transactionsTable, frame);
    }

    private JTable createTransactionsTable() {
        var transactionTableModel = new TransactionTableModel();
        BusinessLogicImpl.getInstance().addTransactionListener(transactionTableModel);
        var table = new JTable(transactionTableModel);
        table.getSelectionModel().addListSelectionListener(this::rowSelectionChanged);
        table.setDefaultRenderer(Object.class, new TransactionTableRenderer(transactionTableModel, table.getColumnCount()));
        table.setShowHorizontalLines(true);
        table.setShowVerticalLines(true);
        table.setRowSorter(createTransactionsSorter(transactionTableModel));

        table.getInputMap().put(KeyStroke.getKeyStroke("DELETE"), "delete_transactions_id");
        table.getActionMap().put("delete_transactions_id", new DeleteTransactionAction(frame, table));

        return table;
    }

    private CategoriesTab createCategoriesTab() {
        categoriesTable.setComponentPopupMenu(createCategoriesTablePopupMenu());
        return new CategoriesTab(categoriesTable);
    }

    private JTable createCategoriesTable() {
        var categoryTableModel = new CategoryTableModel();
        BusinessLogicImpl.getInstance().addCategoryListener(categoryTableModel);
        var table = new JTable(categoryTableModel);
        table.getSelectionModel().addListSelectionListener(this::rowSelectionChanged);
        table.setDefaultRenderer(Object.class, new CategoryTableRenderer(categoryTableModel));
        table.setShowHorizontalLines(true);
        table.setShowVerticalLines(true);
        table.setRowSorter(createCategoriesSorter(categoryTableModel));

        table.getInputMap().put(KeyStroke.getKeyStroke("DELETE"), "delete_categories_id");
        table.getActionMap().put("delete_categories_id", new DeleteCategoryAction(frame, table));

        return table;
    }

    private JPopupMenu createTransactionsTablePopupMenu() {
        var popupMenu = new JPopupMenu();

        popupMenu.add(addTransactionAction);
        popupMenu.add(editTransactionAction);
        popupMenu.add(deleteTransactionAction);

        return popupMenu;
    }

    private JPopupMenu createCategoriesTablePopupMenu() {
        var popupMenu = new JPopupMenu();

        popupMenu.add(addCategoryAction);
        popupMenu.add(editCategoryAction);
        popupMenu.add(deleteCategoryAction);

        return popupMenu;
    }

    private JPanel createTopPanel() {
        var top = new JPanel(new BorderLayout());
        JLabel totalBalanceLabel = new JLabel();
        totalBalanceLabel.setBorder(new EmptyBorder(0,0,0,20));
        var totalBalanceWrapper = new BalanceWrapper(totalBalanceLabel, "Total");
        BalanceCalculator totalBalanceComputer = new BalanceCalculator(totalBalanceWrapper);
        BusinessLogicImpl.getInstance().addTransactionListenerOfAll(totalBalanceComputer);
        top.add(createToolbar(), BorderLayout.WEST);
        top.add(totalBalanceLabel, BorderLayout.EAST);
        return top;
    }

    private JTabbedPane createTabPane() {
        var tabbed = new JTabbedPane(JTabbedPane.TOP);
        tabbed.addTab("Transactions", transactionsTab.getPanel());
        tabbed.addTab("Categories", categoriesTab.getPanel());
        tabbed.setTabLayoutPolicy(JTabbedPane.SCROLL_TAB_LAYOUT);
        return tabbed;
    }

    private JMenuBar createMenuBar() {
        var menuBar = new JMenuBar();
        var fileMenu = new JMenu("File");
        var currenciesMenuItem = new JMenuItem("Currencies");
        currenciesMenuItem.addActionListener(this::openCurrenciesDialog);
        fileMenu.add(settingsAction);
        fileMenu.addSeparator();
        fileMenu.add(currenciesMenuItem);
        fileMenu.addSeparator();

        var importMenu = new JMenu("Import");
        importMenu.add(importCSVAction);
        importMenu.add(importJSONAction);
        importMenu.add(importXMLAction);
        fileMenu.add(importMenu);

        var exportMenu = new JMenu("Export");
        exportMenu.add(exportCSVAction);
        exportMenu.add(exportJSONAction);
        exportMenu.add(exportXMLAction);
        fileMenu.add(exportMenu);

        menuBar.add(fileMenu);

        var helpMenu = new JMenu("Help");
        helpMenu.add(aboutAction);
        menuBar.add(helpMenu);

        return menuBar;
    }

    private JToolBar createToolbar() {
        var toolbar = new JToolBar();

        var transactionsButton = new JButton("Transaction  +");
        toolbar.add(transactionsButton);
        transactionsButton.setBorderPainted(false);
        transactionsButton.addActionListener(addTransactionAction);
        toolbar.addSeparator();

        var categoryButton = new JButton("Category  +");
        toolbar.add(categoryButton);
        categoryButton.setBorderPainted(false);
        categoryButton.addActionListener(addCategoryAction);
        toolbar.addSeparator();

        return toolbar;
    }

    private TableRowSorter<TableModel> createCategoriesSorter(CategoryTableModel categoryTableModel) {
        return new TableRowSorter<>(categoryTableModel);
    }

    private TableRowSorter<TransactionTableModel> createTransactionsSorter(TransactionTableModel transactionTableModel) {
        //Dumb Sorter
        var sorter = new TableRowSorter<>(transactionTableModel);
        sorter.setComparator(1, (Comparator<String>) (str1, str2) -> {
            String[] str1values = str1.split(" ");
            String[] str2values = str2.split(" ");
            Double value1 = Double.parseDouble(str1values[0].replace(',', '.'));
            Double value2 = Double.parseDouble(str2values[0].replace(',', '.'));
            Double rate1 = BusinessLogicImpl.getInstance().getCurrencyRepository().getCurrency(str1values[1]).getConversionRate();
            Double rate2 = BusinessLogicImpl.getInstance().getCurrencyRepository().getCurrency(str2values[1]).getConversionRate();
            return Double.compare(value1 / rate1, value2 / rate2);
        });

        return sorter;
    }

    private void openCurrenciesDialog(ActionEvent e) {
        var parent = (JComponent) tabbedPane.getSelectedComponent();
        var dialog = new CurrenciesDialog(parent);
        dialog.show(parent);
    }

    private void rowSelectionChanged(ListSelectionEvent listSelectionEvent) {
        var selectionModel = (ListSelectionModel) listSelectionEvent.getSource();
        var count = selectionModel.getSelectedItemsCount();
        changeActionsState(count);
    }

    private void changeActionsState(int selectedItemsCount) {
        editCategoryAction.setEnabled(selectedItemsCount == 1);
        editTransactionAction.setEnabled(selectedItemsCount == 1);
        deleteCategoryAction.setEnabled(selectedItemsCount >= 1);
        deleteTransactionAction.setEnabled(selectedItemsCount >= 1);
    }
}
