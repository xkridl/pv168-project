package cz.muni.fi.pv168.seminar02.team4.data.validation.common;

import cz.muni.fi.pv168.seminar02.team4.data.validation.ValidationResult;

public class PositiveNumberValidator extends PropertyValidator<Double> {
    public PositiveNumberValidator(String name) {
        super(name);
    }
    @Override
    public ValidationResult validate(Double number) {
        var result = new ValidationResult();

        if ( number <= 0) {
            result.add("'%s' must be greater than zero."
                    .formatted(name)
            );
        }

        return result;
    }
}
