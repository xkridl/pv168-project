package cz.muni.fi.pv168.seminar02.team4.ui.action;

import cz.muni.fi.pv168.seminar02.team4.data.storage.DataStorageException;
import cz.muni.fi.pv168.seminar02.team4.logic.BusinessLogicImpl;
import cz.muni.fi.pv168.seminar02.team4.model.Transaction;
import cz.muni.fi.pv168.seminar02.team4.model.ValidationInfo;
import cz.muni.fi.pv168.seminar02.team4.ui.dialog.TransactionDialog;
import cz.muni.fi.pv168.seminar02.team4.ui.model.TransactionTableModel;
import cz.muni.fi.pv168.seminar02.team4.ui.resources.Icons;
import org.tinylog.Logger;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.util.Optional;

public class EditTransactionAction extends AbstractAction implements Action {
    private final Component parentComponent;
    private final JTable transactionTable;

    public EditTransactionAction(Component parentComponent, JTable transactionTable) {
        super("Edit", Icons.EDIT_ICON);
        this.parentComponent = parentComponent;
        this.transactionTable = transactionTable;

        putValue(SHORT_DESCRIPTION, "Edits selected transaction");
        putValue(MNEMONIC_KEY, KeyEvent.VK_E);
        putValue(ACCELERATOR_KEY, KeyStroke.getKeyStroke("ctrl E"));
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        var selectedRows = transactionTable.getSelectedRows();
        if (selectedRows.length != 1) {
            throw new IllegalStateException("Invalid selected rows count (must be 1): " + selectedRows.length);
        }
        if (transactionTable.isEditing()) {
            transactionTable.getCellEditor().cancelCellEditing();
        }
        var transactionTableModel = (TransactionTableModel) transactionTable.getModel();
        var selectedRow = selectedRows[0];
        var modelRow = transactionTable.convertRowIndexToModel(selectedRow);
        var transaction = transactionTableModel.getEntityAtIndex(modelRow);
        var oldTransaction = transaction.createSnapshot();
        var transactionDialog = new TransactionDialog(transaction, "Edit transaction");

        transactionDialog.getPanel().setPreferredSize(transactionDialog.getPanel().getPreferredSize());
        Optional<Transaction> editedTransaction;

        while (true) {
            try {
                editedTransaction = transactionDialog.show(parentComponent);
            } catch (NumberFormatException ex) {
                JOptionPane.showMessageDialog(parentComponent,
                        "Bad format of Transaction amount.",
                        "Transaction", JOptionPane.ERROR_MESSAGE);
                continue;
            }

            if (editedTransaction.isEmpty()) {
                transaction.setAttributes(oldTransaction);
                return;
            }

            transaction = editedTransaction.get();
            ValidationInfo validationInfo = transaction.validate();
            if (validationInfo.isInvalid()) {
                JOptionPane.showMessageDialog(parentComponent,
                        validationInfo.getMessage(),
                        "Transaction", JOptionPane.ERROR_MESSAGE);
            } else {
                try {
                    Logger.debug("Editing transaction: " + transaction.detailedPrint());
                    BusinessLogicImpl.getInstance().editTransaction(transaction);
                    return;
                } catch (DataStorageException ex) {
                    Logger.error(ex);
                    JOptionPane.showMessageDialog(parentComponent,
                            "Unable to edit the transaction. Make sure all parameters are valid.",
                            "Transaction", JOptionPane.ERROR_MESSAGE);
                    transaction.setAttributes(oldTransaction);
                }
            }
        }
    }
}
