package cz.muni.fi.pv168.seminar02.team4.data.storage.entity;

import cz.muni.fi.pv168.seminar02.team4.model.Category;

/**
 * {@link Category} representation in the persistent layer.
 */
public record CategoryEntity(
        Long id,
        String name,
        Integer colorRed,
        Integer colorGreen,
        Integer colorBlue
) {
    public CategoryEntity(
            String name,
            Integer colorRed,
            Integer colorGreen,
            Integer colorBlue) {
        this(null, name, colorRed, colorGreen, colorBlue);
    }
}
