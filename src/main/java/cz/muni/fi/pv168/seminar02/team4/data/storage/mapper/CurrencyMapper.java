package cz.muni.fi.pv168.seminar02.team4.data.storage.mapper;

import cz.muni.fi.pv168.seminar02.team4.data.storage.entity.CurrencyEntity;
import cz.muni.fi.pv168.seminar02.team4.data.validation.Validator;
import cz.muni.fi.pv168.seminar02.team4.model.Currency;

/**
 * Mapper from {@link CurrencyEntity} to {@link Currency}.
 */
public class CurrencyMapper implements EntityMapper<CurrencyEntity, Currency> {
    private final Validator<Currency> currencyValidator;

    public CurrencyMapper(Validator<Currency> currencyValidator) {
        this.currencyValidator = currencyValidator;
    }

    @Override
    public CurrencyEntity mapToEntity(Currency model) {
        currencyValidator.validate(model).intoException();

        return new CurrencyEntity(
                model.getId(),
                model.getName(),
                model.getAbbreviation(),
                model.getConversionRate()
        );
    }

    @Override
    public Currency mapToModel(CurrencyEntity entity) {
        return new Currency(
                entity.id(),
                entity.name(),
                entity.abbreviation(),
                entity.conversionRate()
        );
    }
}
