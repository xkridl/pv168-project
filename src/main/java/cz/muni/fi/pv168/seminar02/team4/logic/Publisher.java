package cz.muni.fi.pv168.seminar02.team4.logic;

import cz.muni.fi.pv168.seminar02.team4.data.storage.repository.Repository;
import cz.muni.fi.pv168.seminar02.team4.ui.model.EntityTableModel;

import java.util.Collection;

/**
 * Generic publisher interface for using observer pattern between
 * {@link Repository<E>} and {@link EntityTableModel<E>}.
 *
 * @author Adam Kridl
 */
public interface Publisher<E> {
    void notifyListeners(Collection<E> data);
    void addListener(Listener<E> listener);
    void removeListener(Listener<E> listener);
}
