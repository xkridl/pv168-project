package cz.muni.fi.pv168.seminar02.team4.ui.dialog;

import cz.muni.fi.pv168.seminar02.team4.logic.BusinessLogicImpl;
import cz.muni.fi.pv168.seminar02.team4.model.Amount;
import cz.muni.fi.pv168.seminar02.team4.model.Category;
import cz.muni.fi.pv168.seminar02.team4.model.Currency;
import cz.muni.fi.pv168.seminar02.team4.model.Transaction;
import cz.muni.fi.pv168.seminar02.team4.model.enums.Period;
import cz.muni.fi.pv168.seminar02.team4.model.enums.TransactionType;
import cz.muni.fi.pv168.seminar02.team4.ui.model.ComboBoxModelAdapter;
import cz.muni.fi.pv168.seminar02.team4.ui.model.ListeningListModel;
import cz.muni.fi.pv168.seminar02.team4.ui.model.ListeningListModelImpl;
import cz.muni.fi.pv168.seminar02.team4.ui.model.LocalDateModel;
import net.miginfocom.swing.MigLayout;
import org.jdatepicker.DateModel;
import org.jdatepicker.JDatePicker;

import javax.swing.ButtonGroup;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.Objects;


/**
 * Dialog for adding new transaction.
 */
public class TransactionDialog extends EntityDialog<Transaction> {
    private final Transaction transaction;
    private final JTextField nameField;
    private final JTextField descriptionField;
    private final JTextField amountField;
    private final JComboBox<Currency> currencyComboBox;
    private final DateModel<LocalDate> dateModel;
    private final JComboBox<Category> categoryComboBox;
    private final JRadioButton incomingButton;
    private final JRadioButton outgoingButton;
    private final JCheckBox repetitionCheckbox;
    private final JComboBox<Period> periodCombobox;

    public TransactionDialog(Transaction transaction, String constraints, String title) {
        super(constraints, title);
        this.transaction = initializeTransaction(transaction);

        nameField = new JTextField();
        descriptionField = new JTextField();
        amountField = new JTextField();
        amountField.setBackground(BusinessLogicImpl.getInstance().getTextFieldMainColor());
        ListeningListModel<Currency> currencyModel = new ListeningListModelImpl<>();
        BusinessLogicImpl.getInstance().addCurrencyListener(currencyModel);
        currencyComboBox = new JComboBox<>(new ComboBoxModelAdapter<>(currencyModel));
        dateModel = new LocalDateModel();
        ListeningListModel<Category> categoryModel = new ListeningListModelImpl<>();
        BusinessLogicImpl.getInstance().addCategoryListener(categoryModel);
        categoryComboBox = new JComboBox<>(new ComboBoxModelAdapter<>(categoryModel));
        categoryComboBox.setSelectedIndex(categoryModel.getSize() - 1);
        outgoingButton = new JRadioButton("Outgoing");
        incomingButton = new JRadioButton("Incoming");
        repetitionCheckbox = createRepetitionCheckbox();
        periodCombobox = new JComboBox<>(Arrays.stream(Period.values()).filter(p -> p != Period.ANY).toArray(Period[]::new));
        setDialogItemValues();
        addDialogItems();
    }

    public TransactionDialog(String constraints, String title) {
        this(null, constraints, title);
    }

    public TransactionDialog(String title) {
        this("", title);
    }

    public TransactionDialog(Transaction transaction, String title) {
        this(transaction, "", title);
    }

    private Transaction initializeTransaction(Transaction transaction) {
        return Objects.requireNonNullElseGet(transaction,
                () -> new Transaction("",
                        "",
                        new Amount(0.0d, new Currency("Euro", "EUR", 1.0d)),
                        LocalDate.now(),
                        new Category("", new Color(0, 0, 0, 0)),
                        TransactionType.OUTGOING));
    }

    private JPanel createAmountPanel() {
        amountField.addKeyListener(floatFieldListener(amountField));
        JPanel panelAmount = new JPanel(new MigLayout());
        panelAmount.add(amountField, "w 150%, " + BorderLayout.WEST);
        panelAmount.add(currencyComboBox);
        return panelAmount;
    }
    private JPanel createTypePanel() {
        JPanel panelType = new JPanel();
        ButtonGroup typeButtonGroup = new ButtonGroup();
        typeButtonGroup.add(outgoingButton);
        typeButtonGroup.add(incomingButton);
        panelType.add(outgoingButton);
        panelType.add(incomingButton);

        return panelType;
    }

    private JCheckBox createRepetitionCheckbox() {
        var checkbox = new JCheckBox("Yes", transaction.isRepeating());
        checkbox.addActionListener(this::updatePeriodVisibility);
        return checkbox;
    }

    private void updatePeriodVisibility(ActionEvent e) {
        var checkbox = (JCheckBox) e.getSource();
        periodCombobox.setSelectedIndex(0);
        periodCombobox.setEnabled(checkbox.isSelected());
    }

    void setDialogItemValues() {
        nameField.setText(transaction.getName());
        descriptionField.setText(transaction.getDescription());
        amountField.setText(String.format("%.2f", transaction.getAmount().getAmount()));
        currencyComboBox.setSelectedItem(transaction.getAmount().getCurrency());
        dateModel.setValue(transaction.getDate());
        categoryComboBox.setSelectedItem(transaction.getCategory());
        outgoingButton.setSelected(transaction.isOutgoing());
        incomingButton.setSelected(transaction.isIncoming());
        periodCombobox.setSelectedItem(transaction.getPeriod());
        periodCombobox.setEnabled(transaction.isRepeating());
    }

    @Override
    public void addDialogItems() {
        add("Name", nameField, true, "wrap, grow");
        add("Description", descriptionField, false, "wrap, grow");
        add("Amount", createAmountPanel(), true, "wrap");
        add("Date", new JDatePicker(dateModel), true, "wrap");
        add("Category", categoryComboBox, false, "wrap, w 100%");
        add("Type", createTypePanel(), true, "wrap");
        add("Is repeating", repetitionCheckbox, false, "wrap");
        add("Period", periodCombobox, false, "wrap, grow");
    }

    @Override
    Transaction getEntity() {
        transaction.setName(nameField.getText());
        transaction.setDescription(descriptionField.getText());
        transaction.setAmount(
                new Amount(Double.parseDouble(amountField.getText().replace(",", ".")),
                        (Currency) currencyComboBox.getSelectedItem())
        );
        transaction.setDate(dateModel.getValue());
        transaction.setCategory((Category) categoryComboBox.getSelectedItem());
        transaction.setTransactionType(outgoingButton.isSelected() ? TransactionType.OUTGOING : TransactionType.INCOMING);
        transaction.setPeriod(repetitionCheckbox.isSelected() ? (Period) periodCombobox.getSelectedItem() : null);
        return transaction;
    }
}
