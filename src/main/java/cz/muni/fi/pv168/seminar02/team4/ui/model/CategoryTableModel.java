package cz.muni.fi.pv168.seminar02.team4.ui.model;

import cz.muni.fi.pv168.seminar02.team4.model.Category;

import java.util.List;

public class CategoryTableModel extends EntityTableModel<Category> {
    public CategoryTableModel() {
        super(List.of(
                Column.readonly("Name", String.class, Category::getName)
        ));
    }
}
