package cz.muni.fi.pv168.seminar02.team4.ui.action;

import cz.muni.fi.pv168.seminar02.team4.data.storage.DataStorageException;
import cz.muni.fi.pv168.seminar02.team4.logic.BusinessLogicImpl;
import cz.muni.fi.pv168.seminar02.team4.ui.model.TransactionTableModel;
import cz.muni.fi.pv168.seminar02.team4.ui.resources.Icons;
import org.tinylog.Logger;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.KeyStroke;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.util.Arrays;

public class DeleteTransactionAction extends AbstractAction implements Action {
    private final JTable transactionTable;

    private Component parentComponent;
    public DeleteTransactionAction(Component parentComponent, JTable transactionTable) {
        super("Delete", Icons.DELETE_TRANSACTION_ICON);
        this.transactionTable = transactionTable;
        this.parentComponent = parentComponent;

        putValue(SHORT_DESCRIPTION, "Deletes selected transaction/-s");
        putValue(MNEMONIC_KEY, KeyEvent.VK_D);
        putValue(ACCELERATOR_KEY, KeyStroke.getKeyStroke("DELETE"));
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        var transactionTableModel = (TransactionTableModel) transactionTable.getModel();
        var transactionsToDelete = Arrays.stream(transactionTable.getSelectedRows())
                .mapToObj(transactionTableModel::getEntityAtIndex)
                .toList();

        if (transactionsToDelete.size() > 1) {
            Object[] options = {"Yes", "No"};
            var option = JOptionPane.showOptionDialog(parentComponent,
                    "Multiple transactions are selected. " +
                            "Do you really want to delete all selected transactions?",
                    "Delete transaction", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE,
                    null, options, options[1]);
            if (option != JOptionPane.YES_OPTION) {
                return;
            }
        }

        try {
            Logger.debug("Going to delete the following transactions:");
            transactionsToDelete.forEach(category -> Logger.debug(category.detailedPrint()));
            BusinessLogicImpl.getInstance().deleteTransactions(transactionsToDelete);
            Logger.debug("All transactions were successfully deleted");
        } catch (DataStorageException ex) {
            Logger.error(ex);
        }
    }
}
