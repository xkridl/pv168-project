package cz.muni.fi.pv168.seminar02.team4.wiring;

public interface DependencyProvider {
    void initializeBusinessLogic();
}
