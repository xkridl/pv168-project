package cz.muni.fi.pv168.seminar02.team4.data.manipulation;

import cz.muni.fi.pv168.seminar02.team4.model.Amount;
import cz.muni.fi.pv168.seminar02.team4.model.Category;
import cz.muni.fi.pv168.seminar02.team4.model.Transaction;
import cz.muni.fi.pv168.seminar02.team4.model.enums.Period;
import cz.muni.fi.pv168.seminar02.team4.model.enums.TransactionType;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import javax.swing.*;
import java.awt.*;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

class JsonTransactionImporter implements TransactionImporter {
    private final Component parentComponent;
    public JsonTransactionImporter(Component parentComponent) {
        this.parentComponent = parentComponent;
    }

    private static Transaction parseJsonObject(JSONObject transaction) {
        return new Transaction(
                transaction.get("Name").toString(),
                transaction.get("Description").toString(),
                Amount.parse(transaction.get("Amount").toString()),
                LocalDate.parse(transaction.get("Date").toString()),
                Category.parse(transaction.get("Category").toString()),
                transaction.get("Type").toString().equals("Incoming") ? TransactionType.INCOMING : TransactionType.OUTGOING,
                transaction.get("Period").toString().equals("") ? null : Period.parse(transaction.get("Period").toString()));
    }

    @Override
    public Collection<Transaction> importTransactions(String filePath) {
        try (var reader = new FileReader(filePath, StandardCharsets.UTF_8)) {
            List<Transaction> transactions = new ArrayList<>();
            JSONParser jsonParser = new JSONParser();

            JSONObject jsonObject = (JSONObject) jsonParser.parse(reader);
            JSONArray jsonTransactions = (JSONArray) jsonObject.get("transactions");

            for (var transaction : jsonTransactions) {
                transactions.add(parseJsonObject((JSONObject) transaction));
            }

            return transactions;
        } catch (FileNotFoundException exception) {
            JOptionPane.showMessageDialog(parentComponent,
                    "File does not exist.",
                    "Import", JOptionPane.ERROR_MESSAGE);
            throw new DataManipulationException("File does not exist", exception);
        } catch (IOException exception) {
            JOptionPane.showMessageDialog(parentComponent,
                    "Unable to read file.",
                    "Import", JOptionPane.ERROR_MESSAGE);
            throw new DataManipulationException("Unable to read file", exception);
        } catch (IllegalArgumentException | ParseException exception) {
            JOptionPane.showMessageDialog(parentComponent,
                    "Invalid data structure.",
                    "Import", JOptionPane.ERROR_MESSAGE);
            throw new DataManipulationException("Invalid data structure.", exception);
        }
    }
}
