package cz.muni.fi.pv168.seminar02.team4.model.enums;

import org.apache.commons.lang3.StringUtils;

public enum TransactionType {
    INCOMING,
    OUTGOING;


    @Override
    public String toString() {
        return StringUtils.capitalize(super.toString().toLowerCase());
    }
}
