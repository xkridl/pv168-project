package cz.muni.fi.pv168.seminar02.team4.ui.action;

import cz.muni.fi.pv168.seminar02.team4.ui.dialog.SettingDialog;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;

public class SettingsAction extends AbstractAction {

    private JFrame frame;
    public SettingsAction(JFrame frame) {
        super("Settings", null);
        this.frame = frame;
        putValue(SHORT_DESCRIPTION, "Settings");
        putValue(MNEMONIC_KEY, KeyEvent.VK_S);
        putValue(ACCELERATOR_KEY, KeyStroke.getKeyStroke("ctrl S"));
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        SettingDialog dialog = new SettingDialog("wrap", "Settings", frame);
        dialog.addDialogItems();
        dialog.show(new String[] {"Close"});
    }
}
