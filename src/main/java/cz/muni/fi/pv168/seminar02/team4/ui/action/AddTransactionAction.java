package cz.muni.fi.pv168.seminar02.team4.ui.action;

import cz.muni.fi.pv168.seminar02.team4.data.storage.DataStorageException;
import cz.muni.fi.pv168.seminar02.team4.logic.BusinessLogicImpl;
import cz.muni.fi.pv168.seminar02.team4.model.Transaction;
import cz.muni.fi.pv168.seminar02.team4.model.ValidationInfo;
import cz.muni.fi.pv168.seminar02.team4.ui.dialog.TransactionDialog;
import cz.muni.fi.pv168.seminar02.team4.ui.resources.Icons;
import org.tinylog.Logger;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.util.Optional;

public class AddTransactionAction extends AbstractAction implements Action {
    private final Component parentComponent;
    public AddTransactionAction(Component parentComponent) {
        super("Add", Icons.ADD_TRANSACTION_ICON);
        this.parentComponent = parentComponent;

        putValue(SHORT_DESCRIPTION, "Adds new transaction");
        putValue(MNEMONIC_KEY, KeyEvent.VK_A);
        putValue(ACCELERATOR_KEY, KeyStroke.getKeyStroke("ctrl N"));
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        var transactionDialog = new TransactionDialog("New transaction");
        transactionDialog.getPanel().setPreferredSize(transactionDialog.getPanel().getPreferredSize());
        Optional<Transaction> newTransaction;

        while (true) {
            try {
                newTransaction = transactionDialog.show(parentComponent);
            } catch (NumberFormatException ex) {
                JOptionPane.showMessageDialog(parentComponent,
                        "Bad format of Transaction amount.",
                        "Transaction", JOptionPane.ERROR_MESSAGE);
                continue;
            }

            if (newTransaction.isEmpty()) {
                return;
            }

            var transaction = newTransaction.get();
            ValidationInfo validationInfo = transaction.validate();
            if (validationInfo.isInvalid()) {
                JOptionPane.showMessageDialog(parentComponent,
                        validationInfo.getMessage(),
                        "Transaction", JOptionPane.ERROR_MESSAGE);
            } else {
                try {
                    Logger.debug("Adding transaction: {}", transaction.detailedPrint());
                    BusinessLogicImpl.getInstance().addTransaction(transaction);
                    return;
                } catch (DataStorageException ex) {
                    Logger.error(ex);
                    JOptionPane.showMessageDialog(parentComponent,
                            "Unable to add this transaction. Make sure all parameters are valid.",
                            "Transaction", JOptionPane.ERROR_MESSAGE);
                }
            }
        }
    }
}
