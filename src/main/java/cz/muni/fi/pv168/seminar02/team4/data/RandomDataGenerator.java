package cz.muni.fi.pv168.seminar02.team4.data;

import cz.muni.fi.pv168.seminar02.team4.model.Amount;
import cz.muni.fi.pv168.seminar02.team4.model.Category;
import cz.muni.fi.pv168.seminar02.team4.model.Currency;
import cz.muni.fi.pv168.seminar02.team4.model.Transaction;
import cz.muni.fi.pv168.seminar02.team4.model.enums.Period;
import cz.muni.fi.pv168.seminar02.team4.model.enums.TransactionType;

import java.awt.Color;
import java.time.LocalDate;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.time.Month.OCTOBER;
import static java.time.temporal.ChronoUnit.DAYS;

/**
 * Class determined for debugging phase.
 */
public final class RandomDataGenerator {
    private static RandomDataGenerator generator = null;
    private static final List<String> TRANSACTION_NAMES = List.of(
            "Saving for the reserve", "MUNI Bufet", "Ceske drahy", "Billa Namesti Svornosti",
            "Restaurace Rubin"
    );
    private static final List<String> CATEGORY_NAMES = List.of(
            "Family", "Work", "Outdoor", "Healthcare", "Foreign country payment"
    );
    private static final List<Currency> CURRENCIES = List.of(
            new Currency("Euro", "EUR", 1.0d),
            new Currency("Dollar", "USD", 0.97d),
            new Currency("Czech crown", "CZK", 0.04d)
    );

    private static final LocalDate MIN_DATE = LocalDate.of(2022, OCTOBER, 20);
    private static final LocalDate MAX_DATE = LocalDate.of(2022, OCTOBER, 29);
    private static final int COLOR_BOUND = 256;
    private final Random random = new Random(42L);

    private static int count = 0;

    private RandomDataGenerator() {
        // Intentionally made private to prevent instantiation
    }

    public static RandomDataGenerator getInstance() {
        if (generator == null) {
            generator = new RandomDataGenerator();
        }
        return generator;
    }

    public Transaction createTestTransaction() {
        String name = selectRandom(TRANSACTION_NAMES);
        String description = "";
        LocalDate date = selectRandomLocalDate();
        Category category = createTestCategory();
        TransactionType type = selectRandom(List.of(TransactionType.values()));
        Amount amount = new Amount(random.nextDouble(1000), selectRandom(CURRENCIES));
        var isRepeating = random.nextInt(2) == 0;
        if (isRepeating) {
            Period period = selectRandom(List.of(Period.values()));
            if (period == Period.ANY) {
                period = Period.ONE_WEEK;
            }
            return new Transaction(name, description, amount, date, category, type, period);
        }
        return new Transaction(name, description, amount, date, category, type);
    }

    public List<Transaction> createTestTransactions(int count) {
        return Stream
                .generate(this::createTestTransaction)
                .limit(count)
                .collect(Collectors.toList());
    }

    public Category createTestCategory() {
        String name;
        Color color;
        switch (count % 5) {
            case 0 -> {
                name = "Food";
                color = Color.BLUE;
            }
            case 1 -> {
                name = "Healthcare";
                color = Color.RED;
            }
            case 2 -> {
                name = "Culture";
                color = Color.GREEN;
            }
            case 3 -> {
                name = "Sport";
                color = Color.GRAY;
            }
            default -> {
                name = "Living";
                color = Color.YELLOW;
            }
        }
        count += 1;
        return new Category(name, color);
    }

    public List<Category> createTestCategories(int count) {
        return Stream
                .generate(this::createTestCategory)
                .limit(5)
                .collect(Collectors.toList());
    }

    private <T> T selectRandom(List<T> data) {
        int index = random.nextInt(data.size());
        return data.get(index);
    }

    private Color selectRandomColor() {
        var red = random.nextInt(COLOR_BOUND);
        var green = random.nextInt(COLOR_BOUND);
        var blue = random.nextInt(COLOR_BOUND);
        return new Color(red, green, blue);
    }

    private LocalDate selectRandomLocalDate() {
        int maxDays = Math.toIntExact(DAYS.between(
                RandomDataGenerator.MIN_DATE,
                RandomDataGenerator.MAX_DATE) + 1);
        int days = random.nextInt(maxDays);
        return RandomDataGenerator.MIN_DATE.plusDays(days);
    }

    public List<Currency> getCurrencies() {
        return CURRENCIES;
    }
}
