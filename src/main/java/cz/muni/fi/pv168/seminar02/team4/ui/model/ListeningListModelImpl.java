package cz.muni.fi.pv168.seminar02.team4.ui.model;

import javax.swing.AbstractListModel;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Generic class, which creates an abstraction over using listening combo boxes.
 *
 * @param <E> the type of the entities
 * @author Adam Kridl
 */
public class ListeningListModelImpl<E> extends AbstractListModel<E> implements ListeningListModel<E> {
    private List<E> entities;

    public ListeningListModelImpl() {
        entities = new ArrayList<>();
    }

    @Override
    public List<E> getEntities() {
        return entities;
    }

    @Override
    public void setEntities(Collection<E> entities) {
        this.entities = (List<E>) entities;
    }

    @Override
    public void notify(Collection<E> data) {
        setEntities(data);
    }

    @Override
    public int getSize() {
        return entities.size();
    }

    @Override
    public E getElementAt(int index) {
        if (index < 0 || index >= getSize()) {
            throw new IndexOutOfBoundsException("Illegal index " + index);
        }
        return entities.get(index);
    }
}
