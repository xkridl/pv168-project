package cz.muni.fi.pv168.seminar02.team4.logic.balance;

import cz.muni.fi.pv168.seminar02.team4.logic.Listener;
import cz.muni.fi.pv168.seminar02.team4.model.Transaction;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.Collection;

/**
 * Class doing the computation of the balance.
 *
 * @author Adam Kridl
 */
public class BalanceCalculator implements Listener<Transaction> {
    private final BalanceWrapper balanceWrapper;

    public BalanceCalculator(BalanceWrapper balanceWrapper) {
        this.balanceWrapper = balanceWrapper;
    }

    @Override
    public void notify(Collection<Transaction> transactions) {
        balanceWrapper.setBalance(computeBalance(transactions));
    }

    private double computeBalance(Collection<Transaction> transactions) {
        var totalBalance = 0d;
        for (var transaction : transactions) {
            totalBalance += computeTransactionBalance(transaction);
        }
        return totalBalance;
    }

    private double computeTransactionBalance(Transaction transaction) {
        var executionsCount = getTransactionExecutionsCount(transaction);
        var euroAmount = getEuroAmount(transaction);
        var balance = executionsCount * euroAmount;
        return transaction.isIncoming() ? balance : -balance;
    }

    private int getTransactionExecutionsCount(Transaction transaction) {
        if (isAfterToday(transaction.getDate())) {
            return 0;
        }
        else if (transaction.isOrdinary()) {
            return 1;
        }

        var daysBetween = ChronoUnit
                .DAYS
                .between(transaction.getDate(), LocalDate.now());
        var dayPeriod = transaction
                .getPeriod()
                .toDays();
        return (dayPeriod != 0) ? (int) Math.ceil((double) (daysBetween + 1) / dayPeriod) : 1;
    }

    private static boolean isAfterToday(LocalDate date) {
        return date.compareTo(LocalDate.now()) > 0;
    }

    private double getEuroAmount(Transaction transaction) {
        var conversionRate = transaction.getAmount().getCurrency().getConversionRate();
        var amount = transaction.getAmount().getAmount();
        return conversionRate * amount;
    }
}
