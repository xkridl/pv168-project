package cz.muni.fi.pv168.seminar02.team4.model;

import cz.muni.fi.pv168.seminar02.team4.logic.BusinessLogicImpl;
import cz.muni.fi.pv168.seminar02.team4.data.storage.repository.CategoryRepository;

import java.awt.Color;
import java.util.Objects;

public class Category {
    private Long id;
    private String name;
    private Color color;

    public Category(Long id, String name, Color color) {
        this.id = id;
        this.name = name;
        this.color = color;
    }

    public Category(String name, Color color) {
        this(null, name, color);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = Objects.requireNonNull(id);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public Category createSnapshot() {
        return new Category(getName(), getColor());
    }

    public void setAttributes(Category category) {
        setName(category.getName());
        setColor(category.getColor());
    }

    public ValidationInfo validate() {
        if (hasEmptyName()) {
            return new ValidationInfo(true, "Category name is required.");
        }
        if (getName().length() > 50) {
            return new ValidationInfo(true, "Category name cannot be longer than 50 characters.");
        }
        return new ValidationInfo(false, null);
    }

    @Override
    public String toString() {
        return name;
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof Category)) {
            return false;
        }
        return this.name.equals(((Category) obj).name);
    }

    public static Category parse(String input) {
        CategoryRepository catManipul = BusinessLogicImpl.getInstance().getCategoryRepository();
        if (catManipul.findCategory(input) != null) {
            return catManipul.findCategory(input);
        } else {
            var noCategory = catManipul.findCategory("No category");
            assert noCategory != null;
            return noCategory;
        }
    }

    public boolean hasEmptyName() {
        return getName().isEmpty();
    }

    public String detailedPrint() {
        return "Category={"
                + "name: " + name
                + ", (R,G,B): "
                + "("
                + color.getRed()
                + ", " + color.getGreen()
                + ", " + color.getBlue()
                + ")"
                + "}";
    }
}
