package cz.muni.fi.pv168.seminar02.team4.data.validation;

import cz.muni.fi.pv168.seminar02.team4.data.validation.common.PositiveNumberValidator;
import cz.muni.fi.pv168.seminar02.team4.data.validation.common.StringLengthValidator;
import cz.muni.fi.pv168.seminar02.team4.model.Currency;

import java.util.List;

/**
 * Validator of {@link Currency} class.
 */
public class CurrencyValidator implements Validator<Currency> {
    @Override
    public ValidationResult validate(Currency model) {
        var validators = List.of(
                Validator.extracting(
                        Currency::getName, new StringLengthValidator(1, 50, "Currency name")),
                Validator.extracting(
                        Currency::getAbbreviation, new StringLengthValidator(1, 3, "Abbreviation")),
                Validator.extracting(
                        Currency::getConversionRate, new PositiveNumberValidator("ConversionRate"))
        );

        return Validator.compose(validators).validate(model);
    }
}
