package cz.muni.fi.pv168.seminar02.team4.ui.action;

import cz.muni.fi.pv168.seminar02.team4.ui.dialog.AboutDialog;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;

public class AboutAction extends AbstractAction {
    private Component parentComponent;
    public AboutAction(Component parentComponent) {
        super("About", null);
        this.parentComponent = parentComponent;
        putValue(SHORT_DESCRIPTION, "About");
        putValue(MNEMONIC_KEY, KeyEvent.VK_I);
        putValue(ACCELERATOR_KEY, KeyStroke.getKeyStroke("ctrl I"));
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        AboutDialog dialog = new AboutDialog("wrap 1", "About Cash Flow Evidence", parentComponent);
        dialog.addDialogItems();
        dialog.show(new String[] {"Close"});

    }
}
