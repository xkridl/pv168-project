package cz.muni.fi.pv168.seminar02.team4.ui.tabs;

import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.border.EmptyBorder;
import java.awt.BorderLayout;

public class CategoriesTab {
    private final JComponent panel;

    public CategoriesTab(JTable table) {

        panel = new JPanel(false);
        panel.setLayout(new BorderLayout());
        panel.setBorder(new EmptyBorder(10, 20, 0, 20));

        panel.add(new JScrollPane(table), BorderLayout.CENTER);
    }

    public JComponent getPanel() {
        return panel;
    }
}
