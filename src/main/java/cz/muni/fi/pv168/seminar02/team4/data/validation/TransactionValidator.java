package cz.muni.fi.pv168.seminar02.team4.data.validation;

import cz.muni.fi.pv168.seminar02.team4.data.validation.common.PositiveNumberValidator;
import cz.muni.fi.pv168.seminar02.team4.data.validation.common.StringLengthValidator;
import cz.muni.fi.pv168.seminar02.team4.model.Amount;
import cz.muni.fi.pv168.seminar02.team4.model.Currency;
import cz.muni.fi.pv168.seminar02.team4.model.Transaction;

import java.util.List;
import java.util.function.Function;

/**
 * Validator of {@link Transaction} class.
 */
public class TransactionValidator implements Validator<Transaction> {
    @Override
    public ValidationResult validate(Transaction model) {

        var validators = List.of(
                Validator.extracting(
                        Transaction::getName, new StringLengthValidator(1, 50, "Transaction name")),
                Validator.extracting(
                        Transaction::getDescription, new StringLengthValidator(0, 200, "Transaction description")),
                Validator.extracting(
                        Transaction::getAmountValue, new PositiveNumberValidator("Transaction amount"))
        );

        return Validator.compose(validators).validate(model);
    }
}
