package cz.muni.fi.pv168.seminar02.team4.wiring;

import cz.muni.fi.pv168.seminar02.team4.data.storage.dao.CategoryDao;
import cz.muni.fi.pv168.seminar02.team4.data.storage.dao.CurrencyDao;
import cz.muni.fi.pv168.seminar02.team4.data.storage.dao.TransactionDao;
import cz.muni.fi.pv168.seminar02.team4.data.storage.db.DatabaseManager;
import cz.muni.fi.pv168.seminar02.team4.data.storage.mapper.CategoryMapper;
import cz.muni.fi.pv168.seminar02.team4.data.storage.mapper.CurrencyMapper;
import cz.muni.fi.pv168.seminar02.team4.data.storage.mapper.TransactionMapper;
import cz.muni.fi.pv168.seminar02.team4.data.storage.repository.CategoryRepository;
import cz.muni.fi.pv168.seminar02.team4.data.storage.repository.CurrencyRepository;
import cz.muni.fi.pv168.seminar02.team4.data.storage.repository.TransactionRepository;
import cz.muni.fi.pv168.seminar02.team4.data.validation.CategoryValidator;
import cz.muni.fi.pv168.seminar02.team4.data.validation.CurrencyValidator;
import cz.muni.fi.pv168.seminar02.team4.data.validation.TransactionValidator;
import cz.muni.fi.pv168.seminar02.team4.data.validation.Validator;
import cz.muni.fi.pv168.seminar02.team4.logic.BusinessLogicImpl;
import cz.muni.fi.pv168.seminar02.team4.model.Category;
import cz.muni.fi.pv168.seminar02.team4.model.Currency;
import cz.muni.fi.pv168.seminar02.team4.model.Transaction;

import java.awt.Color;

/**
 * Dependency provider common for all environments
 */
public abstract class CommonDependencyProvider implements DependencyProvider {
    public final DatabaseManager databaseManager;

    protected CommonDependencyProvider(DatabaseManager databaseManager) {
        this.databaseManager = databaseManager;
        initializeBusinessLogic();
    }

    @Override
    public void initializeBusinessLogic() {
        Validator<Category> categoryValidator = new CategoryValidator();
        Validator<Currency> currencyValidator = new CurrencyValidator();
        Validator<Transaction> transactionValidator = new TransactionValidator();

        var categoryRepository = new CategoryRepository(
                new CategoryDao(databaseManager::getConnectionHandler),
                new CategoryMapper(categoryValidator)
        );

        var currencyRepository = new CurrencyRepository(
                new CurrencyDao(databaseManager::getConnectionHandler),
                new CurrencyMapper(currencyValidator)
        );

        var transactionRepository = new TransactionRepository(
                new TransactionDao(databaseManager::getConnectionHandler),
                new TransactionMapper(categoryRepository, currencyRepository, transactionValidator)
        );

        new BusinessLogicImpl.BusinessLogicBuilder()
                .withCategoryRepository(categoryRepository)
                .withCurrencyRepository(currencyRepository)
                .withTransactionRepository(transactionRepository)
                .build();
    }
}
