package cz.muni.fi.pv168.seminar02.team4.model;

import cz.muni.fi.pv168.seminar02.team4.logic.BusinessLogicImpl;
import cz.muni.fi.pv168.seminar02.team4.data.storage.repository.CurrencyRepository;

public class Amount {
    private final Double amount;
    private final Currency currency;

    public Amount(Double amount, Currency currency) {
        if (amount == null || currency == null) {
            throw new IllegalArgumentException("Some input is null.");
        }
        this.amount = Math.round(amount*100.0)/100.0;
        this.currency = currency;
    }

    public Double getAmount() {
        return amount;
    }

    public Currency getCurrency() {
        return currency;
    }

    /**
     * Parsing from the format "COST CURRENCY_ABBREV"
     * @param input - input line
     * @return the Amount
     */
    public static Amount parse(String input) {
        CurrencyRepository currencyRepository = BusinessLogicImpl.getInstance().getCurrencyRepository();
        double cost = Double.parseDouble(input.split(" ")[0]);
        Currency currency;
        if (currencyRepository.getCurrency(input.split(" ")[1]) != null) {
            currency = currencyRepository.getCurrency(input.split(" ")[1]);
        } else {
            currency = currencyRepository.getCurrency("EUR");  // default if not found
        }
        return new Amount(cost, currency);
    }

    @Override
    public String toString() {
        return "Amount={"
                + "amount: " + amount
                + ", currency: " + currency
                + "}";
    }
}
