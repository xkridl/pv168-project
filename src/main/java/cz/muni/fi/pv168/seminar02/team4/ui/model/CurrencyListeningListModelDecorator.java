package cz.muni.fi.pv168.seminar02.team4.ui.model;

import cz.muni.fi.pv168.seminar02.team4.model.Currency;

/**
 * Concrete subclass of the {@link ListeningListModelWithAnyDecorator} which works
 * with {@link Currency} type inside.
 *
 * @author Adam Kridl
 */
public class CurrencyListeningListModelDecorator extends ListeningListModelWithAnyDecorator<Currency> {
    public CurrencyListeningListModelDecorator(ListeningListModel<Currency> currencies) {
        super(currencies);
    }

    @Override
    public Currency getElementWithLabelAny() {
        return new Currency("Any", "Any", 0.0d);
    }
}
