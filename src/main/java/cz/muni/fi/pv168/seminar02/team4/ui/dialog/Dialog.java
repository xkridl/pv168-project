package cz.muni.fi.pv168.seminar02.team4.ui.dialog;

import net.miginfocom.swing.MigLayout;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import java.awt.*;

import static javax.swing.JOptionPane.OK_CANCEL_OPTION;
import static javax.swing.JOptionPane.PLAIN_MESSAGE;

public abstract class Dialog {
    protected JPanel panel = new JPanel();
    private final String title;
    private Component parentComponent;

    public Dialog(String constraints, String title, Component parentComponent) {
        panel.setLayout(new MigLayout(constraints));
        this.title = title;
        this.parentComponent = parentComponent;
    }

    public void show() {
        show(new String[]{ "Save", "Cancel" });
    }

    public void show(String[] options) {
        JOptionPane.showOptionDialog(parentComponent, panel, title,
                OK_CANCEL_OPTION, PLAIN_MESSAGE, null, options, options[0]);
    }

    public abstract void addDialogItems();
}
