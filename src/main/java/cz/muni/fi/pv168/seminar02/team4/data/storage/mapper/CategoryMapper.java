package cz.muni.fi.pv168.seminar02.team4.data.storage.mapper;

import cz.muni.fi.pv168.seminar02.team4.data.storage.entity.CategoryEntity;
import cz.muni.fi.pv168.seminar02.team4.data.validation.Validator;
import cz.muni.fi.pv168.seminar02.team4.model.Category;

import java.awt.Color;

/**
 * Mapper from {@link CategoryEntity} to {@link Category}.
 */
public class CategoryMapper implements EntityMapper<CategoryEntity, Category> {
    private final Validator<Category> categoryValidator;

    public CategoryMapper(Validator<Category> categoryValidator) {
        this.categoryValidator = categoryValidator;
    }

    @Override
    public CategoryEntity mapToEntity(Category model) {
        categoryValidator.validate(model).intoException();

        return new CategoryEntity(
                model.getId(),
                model.getName(),
                model.getColor().getRed(),
                model.getColor().getGreen(),
                model.getColor().getBlue()
        );
    }

    @Override
    public Category mapToModel(CategoryEntity entity) {
        return new Category(
                entity.id(),
                entity.name(),
                new Color(entity.colorRed(), entity.colorGreen(), entity.colorBlue())
        );
    }
}
