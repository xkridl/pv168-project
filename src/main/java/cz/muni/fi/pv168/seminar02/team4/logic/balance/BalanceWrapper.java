package cz.muni.fi.pv168.seminar02.team4.logic.balance;

import javax.swing.JLabel;

/**
 * Class for storing the balance and writing it into corresponding JLabel field.
 *
 * @author Adam Kridl
 */
public class BalanceWrapper {
    private Double balance;
    private final JLabel balanceLabel;
    private final String balanceLabelName;

    public BalanceWrapper(JLabel balanceLabel, String labelName) {
        balance = 0d;
        this.balanceLabel = balanceLabel;
        balanceLabelName = labelName;
    }

    public void setBalance(Double balance) {
        this.balance = balance;
        balanceLabel.setText(formatFilteredBalance());
    }

    private String formatFilteredBalance() {
        return String.format("<html><p><span style=\"font-size:16px\">" + balanceLabelName + ": </span>"
                + "<span style=\"font-size:20px\"><strong> %.2f€</strong></span></p></html> ", balance);
    }
}
