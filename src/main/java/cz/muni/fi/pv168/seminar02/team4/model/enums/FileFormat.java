package cz.muni.fi.pv168.seminar02.team4.model.enums;

/**
 * File formats used with importing/exporting.
 *
 * @author Adam Kridl
 */
public enum FileFormat {
    CSV,
    JSON,
    XML;


    @Override
    public String toString() {
        return super.toString().toLowerCase();
    }
}
