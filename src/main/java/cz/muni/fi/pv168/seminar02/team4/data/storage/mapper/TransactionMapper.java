package cz.muni.fi.pv168.seminar02.team4.data.storage.mapper;

import cz.muni.fi.pv168.seminar02.team4.data.storage.entity.TransactionEntity;
import cz.muni.fi.pv168.seminar02.team4.data.storage.repository.Repository;
import cz.muni.fi.pv168.seminar02.team4.data.validation.Validator;
import cz.muni.fi.pv168.seminar02.team4.model.Amount;
import cz.muni.fi.pv168.seminar02.team4.model.Category;
import cz.muni.fi.pv168.seminar02.team4.model.Currency;
import cz.muni.fi.pv168.seminar02.team4.model.Transaction;

/**
 * Mapper from {@link TransactionEntity} to {@link Transaction}.
 */
public class TransactionMapper implements EntityMapper<TransactionEntity, Transaction> {
    private final Repository<Category> categoryRepository;
    private final Repository<Currency> currencyRepository;
    private final Validator<Transaction> transactionValidator;

    public TransactionMapper(
            Repository<Category> categoryRepository,
            Repository<Currency> currencyRepository,
            Validator<Transaction> transactionValidator) {
        this.categoryRepository = categoryRepository;
        this.currencyRepository = currencyRepository;
        this.transactionValidator = transactionValidator;
    }

    @Override
    public TransactionEntity mapToEntity(Transaction model) {
        transactionValidator.validate(model).intoException();

        return new TransactionEntity(
                model.getId(),
                model.getCategory().getId(),
                model.getAmount().getCurrency().getId(),
                model.getName(),
                model.getDescription(),
                model.getAmountValue(),
                model.getDate(),
                model.getTransactionType(),
                model.getPeriod());
    }

    @Override
    public Transaction mapToModel(TransactionEntity entity) {
        Currency currency = currencyRepository.findById(entity.currencyId()).orElseThrow();
        Category category = categoryRepository.findById(entity.categoryId()).orElseThrow();

        return new Transaction(
                entity.id(),
                entity.name(),
                entity.description(),
                new Amount(entity.amount(), currency),
                entity.date(),
                category,
                entity.transactionType(),
                entity.period()
        );
    }
}
