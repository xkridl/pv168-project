package cz.muni.fi.pv168.seminar02.team4.data.storage.entity;

import cz.muni.fi.pv168.seminar02.team4.model.Currency;

/**
 * {@link Currency} representation in the persistent layer.
 */
public record CurrencyEntity (
        Long id,
        String name,
        String abbreviation,
        Double conversionRate
) {
    public CurrencyEntity(String name, String abbreviation, Double conversionRate) {
        this(null, name, abbreviation, conversionRate);
    }
}
