package cz.muni.fi.pv168.seminar02.team4.data.manipulation;

import cz.muni.fi.pv168.seminar02.team4.model.Amount;
import cz.muni.fi.pv168.seminar02.team4.model.Category;
import cz.muni.fi.pv168.seminar02.team4.model.Transaction;
import cz.muni.fi.pv168.seminar02.team4.model.enums.Period;
import cz.muni.fi.pv168.seminar02.team4.model.enums.TransactionType;

import javax.swing.JOptionPane;
import java.awt.*;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

class CsvTransactionImporter implements TransactionImporter {
    private final Component parentComponent;
    public CsvTransactionImporter(Component parentComponent) {
        this.parentComponent = parentComponent;
    }
    private static Transaction parseCSVLine(String line) {
        String[] split = line.split(",");
        for (int i = 0; i < split.length; i++) {
            split[i] = split[i].replaceAll("\"", "");
        }

        return new Transaction(split[0], split[1], Amount.parse(split[2]),
                LocalDate.parse(split[3]), Category.parse(split[4]),
                split[5].equals("Incoming") ? TransactionType.INCOMING : TransactionType.OUTGOING,
                split.length == 7 ? Period.parse(split[6]) : null);
    }

    @Override
    public Collection<Transaction> importTransactions(String filePath) {
        try (var reader = new BufferedReader(new FileReader(filePath, StandardCharsets.UTF_8))) {
            String line;
            List<Transaction> transactions = new ArrayList<>();
            while ((line = reader.readLine()) != null) {
                transactions.add(parseCSVLine(line));
            }
            return transactions;
        } catch (FileNotFoundException exception) {
            JOptionPane.showMessageDialog(parentComponent,
                    "File does not exist.",
                    "Import", JOptionPane.ERROR_MESSAGE);
            throw new DataManipulationException("File does not exist", exception);
        } catch (IOException exception) {
            JOptionPane.showMessageDialog(parentComponent,
                    "Unable to read file.",
                    "Import", JOptionPane.ERROR_MESSAGE);
            throw new DataManipulationException("Unable to read file", exception);
        } catch (IllegalArgumentException exception) {
            JOptionPane.showMessageDialog(parentComponent,
                    "Invalid data structure.",
                    "Import", JOptionPane.ERROR_MESSAGE);
            throw new DataManipulationException("Invalid data structure.", exception);
        } catch (Exception exception) {
            JOptionPane.showMessageDialog(parentComponent,
                    "Error has occurred during processing.",
                    "Import", JOptionPane.ERROR_MESSAGE);
            throw new DataManipulationException("Error has occurred during processing.", exception);
        }
    }
}
