package cz.muni.fi.pv168.seminar02.team4.ui.dialog;

import net.miginfocom.swing.MigLayout;

import javax.swing.*;
import java.awt.Component;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.Optional;

import static javax.swing.JOptionPane.OK_CANCEL_OPTION;
import static javax.swing.JOptionPane.OK_OPTION;
import static javax.swing.JOptionPane.PLAIN_MESSAGE;

public abstract class EntityDialog<E> {
    protected final JPanel panel;
    private final String title;

    EntityDialog(String constraints, String title) {
        panel = new JPanel();
        panel.setLayout(new MigLayout(constraints));
        this.title = title;
    }

    void add(String labelText, JComponent component, boolean mandatoryField, String constraints) {
        if (mandatoryField) {
            labelText = "<html>" + labelText + " <font color=#FF0000\">*</font></html>";
        }
        var label = new JLabel(labelText);
        panel.add(label);
        panel.add(component, constraints);
    }

    public Optional<E> show(Component parent) {
        String[] options = {"Save", "Cancel"};
        int result = JOptionPane.showOptionDialog(parent, panel, title,
                OK_CANCEL_OPTION, PLAIN_MESSAGE, null, options, options[0]);
        if (result == OK_OPTION) {
            return Optional.of(getEntity());
        } else {
            return Optional.empty();
        }
    }

    abstract void addDialogItems();
    abstract E getEntity();
    public JPanel getPanel() {
        return panel;
    }
    protected KeyListener floatFieldListener(JTextField field) {
        return new KeyAdapter() {
            public void keyPressed(KeyEvent keyEvent) {
                char keyChar = keyEvent.getKeyChar();
                int keyCode = keyEvent.getKeyCode();
                field.setEditable(Character.isDigit(keyChar) || keyChar == '.' || keyChar == ','
                        || keyCode == KeyEvent.VK_BACK_SPACE || keyCode == KeyEvent.VK_DELETE);
            }
        };
    }
}
