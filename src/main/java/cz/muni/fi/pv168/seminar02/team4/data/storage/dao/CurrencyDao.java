package cz.muni.fi.pv168.seminar02.team4.data.storage.dao;

import cz.muni.fi.pv168.seminar02.team4.data.storage.DataStorageException;
import cz.muni.fi.pv168.seminar02.team4.data.storage.db.ConnectionHandler;
import cz.muni.fi.pv168.seminar02.team4.data.storage.entity.CurrencyEntity;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.function.Supplier;

public class CurrencyDao implements DataAccessObject<CurrencyEntity> {
    private final Supplier<ConnectionHandler> connections;
    public CurrencyDao(Supplier<ConnectionHandler> connections) {
        this.connections = connections;
    }

    @Override
    public CurrencyEntity create(CurrencyEntity entity) {
        var sql = """
            INSERT INTO Currency(name, abbreviation, conversionRate)
            VALUES (?, ?, ?);
            """;

        try (
                var connection = connections.get();
                var statement = connection.use().prepareStatement(sql, Statement.RETURN_GENERATED_KEYS)
        ) {
            statement.setString(1, entity.name());
            statement.setString(2, entity.abbreviation());
            statement.setDouble(3, entity.conversionRate());
            statement.executeUpdate();

            try (ResultSet keyResultSet = statement.getGeneratedKeys()) {
                long currencyId;

                if (keyResultSet.next()) {
                    currencyId = keyResultSet.getLong(1);
                } else {
                    throw new DataStorageException("Failed to fetch generated key for: " + entity);
                }
                if (keyResultSet.next()) {
                    throw new DataStorageException("Multiple keys returned for: " + entity);
                }

                return findById(currencyId).orElseThrow();
            }
        } catch (SQLException ex) {
            throw new DataStorageException("Failed to store: " + entity, ex);
        }
    }

    @Override
    public Collection<CurrencyEntity> findAll() {
        var sql = """
                SELECT id,
                       name,
                       abbreviation,
                       conversionRate
                FROM Currency;
                """;

        try (
                var connection = connections.get();
                var statement = connection.use().prepareStatement(sql)
        ) {
            List<CurrencyEntity> currencies = new ArrayList<>();
            try (var resultSet = statement.executeQuery()) {
                while (resultSet.next()) {
                    var currency = currencyFromResultSet(resultSet);
                    currencies.add(currency);
                }
            }

            return currencies;
        } catch (SQLException ex) {
            throw new DataStorageException("Failed to load all currencies", ex);
        }
    }

    @Override
    public Optional<CurrencyEntity> findById(long id) {
        var sql = """
                SELECT "id",
                       "name",
                       "abbreviation",
                       "conversionRate"
                    FROM Currency
                    WHERE "id" = ?;
                """;

        try (
                var connection = connections.get();
                var statement = connection.use().prepareStatement(sql)
        ) {
            statement.setLong(1, id);
            var resultSet = statement.executeQuery();
            if (resultSet.next()) {
                return Optional.of(currencyFromResultSet(resultSet));
            } else {
                // currency not found
                return Optional.empty();
            }
        } catch (
                SQLException ex) {
            throw new DataStorageException("Failed to load currency by id: " + id, ex);
        }
    }

    @Override
    public CurrencyEntity update(CurrencyEntity entity) {
        var sql = """
                UPDATE Currency
                SET name=?, abbreviation=?, conversionRate=?
                WHERE id=?;
                """;
        try (
                var connection = connections.get();
                var statement = connection.use().prepareStatement(sql)
        ) {
            statement.setString(1, entity.name());
            statement.setString(2, entity.abbreviation());
            statement.setDouble(3, entity.conversionRate());
            statement.setLong(4, entity.id());
            var recordsChanged = statement.executeUpdate();

            if (recordsChanged == 0) {
                throw new DataStorageException("Failed to fetch generated key for: " + entity);
            } else if (recordsChanged > 1) {
                throw new DataStorageException("Multiple keys returned for: " + entity);
            }

            return findById(entity.id()).orElseThrow();
        } catch (SQLException ex) {
            throw new DataStorageException("Failed to update entity: " + entity, ex);
        }
    }

    @Override
    public void deleteById(long entityId) {
        var sql = """
                DELETE FROM Currency
                WHERE id = ?;
                """;

        try (
                var connection = connections.get();
                var statement = connection.use().prepareStatement(sql)
        ) {
            statement.setLong(1, entityId);
            int rowsUpdated = statement.executeUpdate();
            if (rowsUpdated == 0) {
                throw new DataStorageException("Currency not found %d".formatted(entityId));
            }
            if (rowsUpdated > 1) {
                throw new DataStorageException(
                        "More then 1 currency (rows=%d) has been deleted: %d".formatted(rowsUpdated, entityId));
            }
        } catch (SQLException ex) {
            throw new DataStorageException("Failed to delete currency %d".formatted(entityId), ex);
        }
    }

    private static CurrencyEntity currencyFromResultSet(ResultSet resultSet) throws SQLException {
        return new CurrencyEntity(
                resultSet.getLong("id"),
                resultSet.getString("name"),
                resultSet.getString("abbreviation"),
                resultSet.getDouble("conversionRate")
        );
    }
}
