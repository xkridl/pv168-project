package cz.muni.fi.pv168.seminar02.team4.ui.model;

import javax.swing.AbstractListModel;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Generic decorator class which decorates {@link ListeningListModelImpl} in such a way
 * that it adds to the beginning the element with label 'Any'. The purpose is to have opportunity
 * to use such a decorated {@code ListeningListModel} in a {@link javax.swing.JComboBox} afterwards.
 *
 * @param <E> type of the entities
 * @author Adam Kridl
 */
public abstract class ListeningListModelWithAnyDecorator<E> extends AbstractListModel<E> implements ListeningListModel<E> {
    private final ListeningListModel<E> wrapee;

    public ListeningListModelWithAnyDecorator(ListeningListModel<E> wrapee) {
        this.wrapee = wrapee;
    }

    @Override
    public void notify(Collection<E> data) {
        wrapee.notify(getDecoratedEntities((List<E>) data));
    }

    private Collection<E> getDecoratedEntities(List<E> entities) {
        var entitiesWithAny = new ArrayList<>(entities);
        entitiesWithAny.add(0, getElementWithLabelAny());
        return entitiesWithAny;
    }

    @Override
    public Collection<E> getEntities() {
        return wrapee.getEntities();
    }

    @Override
    public void setEntities(Collection<E> entities) {
        wrapee.setEntities(entities);
    }

    @Override
    public int getSize() {
        return wrapee.getSize();
    }

    @Override
    public E getElementAt(int index) {
        return wrapee.getElementAt(index);
    }

    public abstract E getElementWithLabelAny();
}
