package cz.muni.fi.pv168.seminar02.team4.model;

import cz.muni.fi.pv168.seminar02.team4.data.storage.dao.CurrencyDao;
import cz.muni.fi.pv168.seminar02.team4.data.storage.db.DatabaseManager;
import cz.muni.fi.pv168.seminar02.team4.data.storage.mapper.CurrencyMapper;
import cz.muni.fi.pv168.seminar02.team4.data.storage.repository.CategoryRepository;
import cz.muni.fi.pv168.seminar02.team4.data.validation.CurrencyValidator;
import cz.muni.fi.pv168.seminar02.team4.logic.BusinessLogicImpl;
import cz.muni.fi.pv168.seminar02.team4.data.storage.repository.CurrencyRepository;
import cz.muni.fi.pv168.seminar02.team4.model.enums.TransactionType;
import cz.muni.fi.pv168.seminar02.team4.wiring.CommonDependencyProvider;
import cz.muni.fi.pv168.seminar02.team4.wiring.DependencyProvider;
import cz.muni.fi.pv168.seminar02.team4.wiring.ProductionDependencyProvider;
import cz.muni.fi.pv168.seminar02.team4.wiring.TestDependencyProvider;
import org.junit.jupiter.api.*;

import java.time.LocalDate;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatCode;

class AmountTest {
    private TestDependencyProvider dependencyProvider;
    @BeforeEach
    void setUp() {
        this.dependencyProvider = new TestDependencyProvider();
    }
    @AfterEach
    void tearDown() {
        this.dependencyProvider.databaseManager.destroySchema();
    }

    @Test
    public void parseString() {
        CategoryRepository categoryRepository = BusinessLogicImpl.getInstance().getCategoryRepository();
        CurrencyRepository currencyRepository = BusinessLogicImpl.getInstance().getCurrencyRepository();

        assertThat(categoryRepository.getSize()).isGreaterThan(0);
        assertThat(currencyRepository.getSize()).isGreaterThan(0);

        Amount amount1 = Amount.parse("10.10 EUR");
        assertThat(amount1.getAmount()).isEqualTo(10.10d);
        assertThat(amount1.getCurrency().getAbbreviation()).isEqualTo("EUR");
        assertThat(amount1.getCurrency().getConversionRate()).isEqualTo(1.0d);

        Amount amount2 = Amount.parse("10.10 DNS");
        assertThat(amount2.getAmount()).isEqualTo(10.10d);
        assertThat(amount2.getCurrency().getAbbreviation()).isEqualTo("EUR");
        assertThat(amount2.getCurrency().getConversionRate()).isEqualTo(1.0d);

        Assertions.assertThrows(NumberFormatException.class, () -> {
            Amount.parse("10,10 EUR");
        });
    }
}