package cz.muni.fi.pv168.seminar02.team4.data.manipulation;

import cz.muni.fi.pv168.seminar02.team4.data.storage.dao.CurrencyDao;
import cz.muni.fi.pv168.seminar02.team4.data.storage.mapper.CurrencyMapper;
import cz.muni.fi.pv168.seminar02.team4.data.storage.repository.CategoryRepository;
import cz.muni.fi.pv168.seminar02.team4.data.validation.CurrencyValidator;
import cz.muni.fi.pv168.seminar02.team4.logic.BusinessLogicImpl;
import cz.muni.fi.pv168.seminar02.team4.data.storage.repository.CurrencyRepository;
import cz.muni.fi.pv168.seminar02.team4.model.Amount;
import cz.muni.fi.pv168.seminar02.team4.model.Category;
import cz.muni.fi.pv168.seminar02.team4.model.Currency;
import cz.muni.fi.pv168.seminar02.team4.model.Transaction;
import cz.muni.fi.pv168.seminar02.team4.model.enums.Period;
import cz.muni.fi.pv168.seminar02.team4.model.enums.TransactionType;
import cz.muni.fi.pv168.seminar02.team4.wiring.TestDependencyProvider;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.awt.*;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.Instant;
import java.time.LocalDate;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

class XmlTransactionExporterTest {
    private TestDependencyProvider dependencyProvider;
    private static final Path PROJECT_ROOT = Paths.get(System.getProperty("project.basedir", "")).toAbsolutePath();
    private static final Path TEST_RESOURCES = PROJECT_ROOT.resolve(Path.of("src", "test", "resources"));
    private final TransactionExporter exporter = new XmlTransactionExporter(null);
    private final Path exportFilePath = TEST_RESOURCES.resolve(Instant.now().toString().replace(':', '_'));

    @BeforeEach
    void setUp() {
        this.dependencyProvider = new TestDependencyProvider();
    }
    @AfterEach
    void tearDown() {
        this.dependencyProvider.databaseManager.destroySchema();
    }
    @Test
    public void exportXML() throws IOException {
        CurrencyRepository currencyRepository = BusinessLogicImpl.getInstance().getCurrencyRepository();
        assertThat(currencyRepository.getCurrency("EUR")).isNotNull();
        assertThat(currencyRepository.getCurrency("CZK")).isNotNull();

        Amount a1 = new Amount(500d, currencyRepository.getCurrency("EUR"));
        Category c1 = new Category("Live", Color.YELLOW);
        Transaction t1 = new Transaction("Holiday", "Awesome holiday in Croatia",
                a1, LocalDate.parse("2021-10-11"), c1, TransactionType.OUTGOING);

        Amount a2 = new Amount(40d, currencyRepository.getCurrency("CZK"));
        Category c2 = new Category("Food", Color.BLUE);
        Transaction t2 = new Transaction("Kaufland", "For holiday in Croatia",
                a2, LocalDate.parse("2020-11-21"), c2, TransactionType.OUTGOING, Period.ONE_DAY);

        assertThat(t1.getAmount().getCurrency().getAbbreviation()).isEqualTo("EUR");
        assertThat(t2.getAmount().getCurrency().getAbbreviation()).isEqualTo("CZK");

        exporter.exportTransactions(List.of(t1, t2), exportFilePath.toString());

        assertExportedContent("""
                <?xml version="1.0" encoding="UTF-8" standalone="no"?>
                <transactions>
                <transaction>
                <name>Holiday</name>
                <description>Awesome holiday in Croatia</description>
                <amount>500.0 EUR</amount>
                <date>2021-10-11</date>
                <category>Live</category>
                <type>Outgoing</type>
                <period/>
                </transaction>
                <transaction>
                <name>Kaufland</name>
                <description>For holiday in Croatia</description>
                <amount>40.0 CZK</amount>
                <date>2020-11-21</date>
                <category>Food</category>
                <type>Outgoing</type>
                <period>1 day</period>
                </transaction>
                </transactions>
                """);
        Files.deleteIfExists(exportFilePath);
    }

    private void assertExportedContent(String expectedContent) throws IOException {
        assertThat(Files.readString(exportFilePath))
                .isEqualToIgnoringNewLines(expectedContent);
    }
}