package cz.muni.fi.pv168.seminar02.team4.data.manipulation;

import cz.muni.fi.pv168.seminar02.team4.data.storage.dao.CategoryDao;
import cz.muni.fi.pv168.seminar02.team4.data.storage.dao.CurrencyDao;
import cz.muni.fi.pv168.seminar02.team4.data.storage.mapper.CategoryMapper;
import cz.muni.fi.pv168.seminar02.team4.data.storage.mapper.CurrencyMapper;
import cz.muni.fi.pv168.seminar02.team4.data.storage.repository.CategoryRepository;
import cz.muni.fi.pv168.seminar02.team4.data.storage.repository.CurrencyRepository;
import cz.muni.fi.pv168.seminar02.team4.data.validation.CategoryValidator;
import cz.muni.fi.pv168.seminar02.team4.data.validation.CurrencyValidator;
import cz.muni.fi.pv168.seminar02.team4.logic.BusinessLogicImpl;
import cz.muni.fi.pv168.seminar02.team4.model.Category;
import cz.muni.fi.pv168.seminar02.team4.model.Currency;
import cz.muni.fi.pv168.seminar02.team4.model.Transaction;
import cz.muni.fi.pv168.seminar02.team4.ui.model.CategoryTableModel;
import cz.muni.fi.pv168.seminar02.team4.wiring.TestDependencyProvider;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.swing.JTable;
import java.awt.Color;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.util.Collection;

import static org.assertj.core.api.Assertions.assertThat;

class JsonTransactionImporterTest {
    private TestDependencyProvider dependencyProvider;
    private static final Path PROJECT_ROOT = Paths.get(System.getProperty("project.basedir", "")).toAbsolutePath();
    private static final Path TEST_RESOURCES = PROJECT_ROOT.resolve(Path.of("src", "test", "resources"));
    private final TransactionImporter importer = new JsonTransactionImporter(null);

    @BeforeEach
    void setUp() {
        this.dependencyProvider = new TestDependencyProvider();
    }
    @AfterEach
    void tearDown() {
        this.dependencyProvider.databaseManager.destroySchema();
    }
    @Test
    public void singleTransaction() {
        BusinessLogicImpl.getInstance().addCategory(new Category("Food", Color.GREEN));

        Path importFilePath = TEST_RESOURCES.resolve("single-transaction.json");
        Collection<Transaction> transactions = importer.importTransactions(importFilePath.toString());
        assertThat(transactions.size()).isEqualTo(1);

        Transaction[] trs = transactions.toArray(Transaction[]::new);
        Transaction tr = trs[0];
        assertThat(tr.getName()).isEqualTo("cake");
        assertThat(tr.getDate()).isEqualTo(LocalDate.parse("2022-10-20"));
        assertThat(tr.getDescription()).isEqualTo("tasty");
        assertThat(tr.getCategory().getName()).isEqualTo("Food");
        assertThat(tr.getPeriod()).isEqualTo(null);
        assertThat(tr.getAmount().getAmount()).isEqualTo(12.34);
        assertThat(tr.getAmount().getCurrency()).isEqualTo(new Currency("Euro", "EUR", 1.0));
    }
}